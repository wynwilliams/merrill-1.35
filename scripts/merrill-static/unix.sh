#!/usr/bin/env bash

#
# Compile static version of MERRILL.
# OSX builds on OSX.
# Linux builds on Linux.
# Windows builds on Linux with Mingw64.
#


# Exit on first error
set -e

# Directory containing this script
script_dir="$(cd "$(dirname "$(which ${BASH_SOURCE[0]})")"; echo $PWD)"

function help_message() {
    echo "Usage $(basename $0) [--prefix INSTALL_DIR] [--build BUILD_DIR] [--source SOURCE_DIR] [-- [CMake args]]"
    exit 0
}

function error_echo {
    echo "$(basename $0):" "$@" >&2
    exit 1
}

function arg_error() {
    error_echo "Expected argument to $1"
}


while :; do
    case $1 in
        -h|--help)
            help_message
            ;;

        # Prefix argument
        -p|--prefix)
            if [ -n "$2" ]; then
                install_dir="$2"
                shift
            else
                arg_error --prefix
            fi
            ;;
        --prefix=?*)
            install_dir="${1#*=}"
            ;;
        --prefix=)
            arg_error --prefix
            ;;

        # Build argument
        -b|--build)
            if [ -n "$2" ]; then
                build_dir="$2"
                shift
            else
                arg_error --build
            fi
            ;;
        --build=?*)
            build_dir="${1#*=}"
            ;;
        --build=)
            arg_error --build
            ;;

        # Source argument
        -b|--source)
            if [ -n "$2" ]; then
                source_dir="$2"
                shift
            else
                arg_error --source
            fi
            ;;
        --source=?*)
            source_dir="${1#*=}"
            ;;
        --source=)
            arg_error --source
            ;;

        # Platform argument
        --platform)
            if [ -n "$2" ]; then
                platform="$2"
                shift
            else
                arg_error --platform
            fi
            ;;
        --platform=?*)
            platform="${1#*=}"
            ;;
        --platform=)
            arg_error --source
            ;;

        # Rest of arguments passed to CMake
        --)
            shift
            break
            ;;

        # Pass from first unknown argument to CMake.
        *)
            break
            ;;
    esac

    shift
done


#
# Argument defaults
#

# Default build/install dirs
: ${build_dir:=$PWD/build/build/merrill}
: ${install_dir:=$PWD/build/install}
: ${source_dir:=$script_dir/../..}


# Make build/install dirs absolute
mkdir -p "$build_dir"
mkdir -p "$install_dir"

build_dir="$(cd "$build_dir"; echo $PWD)"
install_dir="$(cd "$install_dir"; echo $PWD)"
source_dir="$(cd "$source_dir"; echo $PWD)"


export PATH=$install_dir/bin:$PATH

# Setup default platform
: ${platform:=$(uname)}


# Setup platform specific default compilers and flags
case $platform in
    # Compiling on/for linux
    Linux)
        # Compilers
        : ${FC:=$(which gfortran)}
        : ${AR:=$(which ar)}
        : ${RANLIB:=$(which ranlib)}

        # Add custom libgfortran.spec to replace linking libquadmath.so
        # with libquadmath.a during static linking of libgfortran.
        spec_dir=$build_dir/spec
        echo SPEC DIR: ${spec_dir}
        mkdir -p $spec_dir
        cat > $spec_dir/libgfortran.spec <<EOF
%rename lib liborig
*lib: %{static-libgfortran: --push-state -Bstatic} -lquadmath %{static-libgfortran: --pop-state} -lm %(libgcc) %(liborig)
EOF

        # Setup static flags
        LDFLAGS="-B ${spec_dir} $LDFLAGS"

        LDFLAGS="-static-libgfortran -static-libgcc $LDFLAGS"
        ;;

    # Compiling for windows using mingw64
    Mingw64)
        : ${FC:=$(which x86_64-w64-mingw32-gfortran)}
        # Try to get AR and RANLIB from FC.
        : ${AR:=$($FC -print-prog-name=ar)}
        : ${RANLIB:=$($FC -print-prog-name=ranlib)}

        LDFLAGS="-static $LDFLAGS"
        ;;

    # Compiling on OSX
    Darwin)
        # Compilers
        : ${FC:=$(which gfortran)}
        : ${AR:=$(which ar)}
        : ${RANLIB:=$(which ranlib)}

        # Try to find static libquadmath
        libquadmath=$($FC -print-file-name=libquadmath.a)
        if [ $libquadmath = "libquadmath.a" ]
        then
            echo "Error: Unable to find libquadmath.a."
            echo "Unable to statically link libgfortran. Exiting."
            exit 1
        fi

        # Add custom libgfortran.spec to replace linking libquadmath.so
        # with libquadmath.a during static linking of libgfortran.
        spec_dir=$build_dir/spec
        mkdir -p $spec_dir
        cat > $spec_dir/libgfortran.spec <<EOF
%rename lib liborig
*lib: $libquadmath -lm %(libgcc) %(liborig)
EOF

        LDFLAGS="-B ${spec_dir} $LDFLAGS"

        # Setup static flags
        LDFLAGS="-static-libgfortran -static-libgcc $LDFLAGS"
        ;;
esac



# Export compiler paths
export FC
export AR
export RANLIB



cmake \
    -B"$build_dir" -H"$source_dir" \
    -DCMAKE_INSTALL_PREFIX:PATH="$install_dir" \
    -DCMAKE_Fortran_COMPILER:PATH="$FC" \
    -DCMAKE_Fortran_FLAGS:STRING="$FFLAGS" \
    -DCMAKE_AR:PATH="$AR" \
    -DCMAKE_RANLIB:PATH="$RANLIB" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=OFF \
    -DBUILD_SHARED_LIBS:BOOL=OFF \
    -DCMAKE_BUILD_TYPE:STRING="Release" \
    -DMERRILL_ENABLE_PLUGINS:BOOL=OFF \
    -DCMAKE_EXE_LINKER_FLAGS:STRING="$LDFLAGS" \
    "$@"

cmake --build "$build_dir"
cmake --build "$build_dir" --target install


#
# Copy this build script to share/merrill
#
mkdir -p "${install_dir}/share/merrill"

script_name=$(basename ${BASH_SOURCE[0]})
cat > "${install_dir}/share/merrill/${script_name}" <<EOF
#!/usr/bin/env bash

# Setting defaults from original build arguments

# Default build/install dirs
: \${build_dir:=$build_dir}
: \${install_dir:=$install_dir}
: \${source_dir:=$source_dir}


# Setup default compilers
: \${FC:=$FC}
: \${AR:=$AR}
: \${RANLIB:=$RANLIB}


# MERRILL build stript:
EOF
cat "${BASH_SOURCE[0]}" >> "${install_dir}/share/merrill/${script_name}"
