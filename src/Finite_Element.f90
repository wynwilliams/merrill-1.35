!> This module contains routines and arrays for building FEM representations
!> of the micromagnetic equations.
MODULE Finite_Element
  USE Utils
  IMPLICIT NONE


  REAL(KIND=DP) FEMTolerance

  !
  ! Sparse matrices are constructed in the following format:
  ! For, say, a NNODE x NNODE matrix named PoissonNeumannMatrix with
  ! abbreviation PNM
  !   NNODE   : number of columns
  !   nze_pnm : number of non-zero entries in matrix PNM
  !   PoissonNeumannMatrix(nze_pnm) : entries of the PNM matrix
  !   CNR_PNM(NNODE+1) : column indices of the PNM matrix
  !   RNR_PNM(nze_pnm) : row indices of the PNM matrix
  !
  ! The CNR and RNR indexing isn't obvious.
  ! The value of CNR_PNM(i) contains the starting index for the ith column
  ! of entries in PoissonNeumannMatrix. So the values
  !   PoissonNeumannMatrix(CNR_PNM(i) : CNR_PNM(i+1)-1)
  ! all belong to column i.
  ! The value of RNR_PNM(j) contains the row index of the jth data entry
  ! of PoissonNeumannMatrix.
  ! A matrix-vector multiplication of PNM with a vector X, with result
  ! stored in the vector V, can be written
  !   DO i=1,NNODE
  !     DO j=CNR(i),CNR(i+1)-1
  !       V(i) = V(i) + PoissonNeumannMatrix(j)*X(RNR_PNM(j))
  !     END DO
  !   END DO
  ! 
  ! Matrices constructed from finite element formulation of problem
  ! stored in column major format
  !     PoissonNeumannMatrix(:)
  !     PoissonDirichletMatrix(:)
  !     YA4(:,:)
  !     FAX(:), FAY(:), FAZ(:)
  ! With column and row indices of the form CNRXX and RNRXX, and some
  ! with subdomain indices of the form SDNRXX.

  ! Sparse Matrices for the Poisson Bilinear Form using
  ! Neumann BCs and Dirichlet BCs. Abbreviated PNM and PDM.
  INTEGER :: nze_pnm
  REAL(KIND=DP), ALLOCATABLE :: PoissonNeumannMatrix(:)
  INTEGER, ALLOCATABLE :: RNR_PNM(:), CNR_PNM(:)

  INTEGER, ALLOCATABLE :: Savenze_pnm(:)
  TYPE(SaveDPRow),  ALLOCATABLE :: SavePoissonNeumannMatrix(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveRNR_PNM(:), SaveCNR_PNM(:)


  INTEGER :: nze_pdm
  REAL(KIND=DP), ALLOCATABLE :: PoissonDirichletMatrix(:)
  INTEGER, ALLOCATABLE :: RNR_PDM(:), CNR_PDM(:)

  INTEGER, ALLOCATABLE :: Savenze_pdm(:)
  TYPE(SaveDPRow),  ALLOCATABLE :: SavePoissonDirichletMatrix(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveRNR_PDM(:), SaveCNR_PDM(:)


  ! The ExchangeMatrix is effectively the PoissonNeumannMatrix, but
  ! with subdomains included.
  INTEGER :: nze_em
  REAL(KIND=DP), ALLOCATABLE :: ExchangeMatrix(:)
  INTEGER, ALLOCATABLE :: RNR_EM(:), CNR_EM(:), SDNR_EM(:)

  INTEGER, ALLOCATABLE :: Savenze_em(:)
  TYPE(SaveDPRow),  ALLOCATABLE :: SaveExchangeMatrix(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveRNR_EM(:), SaveCNR_EM(:), SaveSDNR_EM(:)


  ! Sparse matrix for the I grad(m) . v dV RHS of the Poisson
  ! equation with Neumann BCs
  INTEGER :: nze4
  REAL(KIND=DP), ALLOCATABLE :: YA4(:,:)
  REAL(KIND=DP), ALLOCATABLE :: FAX(:), FAY(:), FAZ(:)
  INTEGER, ALLOCATABLE :: RNR4(:), CNR4(:), SDNR4(:)

  INTEGER, ALLOCATABLE :: Savenze4(:)
  TYPE(SaveDPArray), ALLOCATABLE :: SaveYA4(:)
  TYPE(SaveDPRow),   ALLOCATABLE :: SaveFAX(:), SaveFAY(:), SaveFAZ(:)
  TYPE(SaveIntRow),  ALLOCATABLE :: SaveRNR4(:), SaveCNR4(:), SaveSDNR4(:)


  ! The InterpolationMatrix, abbreviated IM.
  ! A multiphase version of vbox.
  ! Sparse matrix for interpolating node-wise expressions with element-wise
  ! material parameters to an energy gradient.
  INTEGER :: nze_im
  REAL(KIND=DP), ALLOCATABLE :: InterpolationMatrix(:)
  INTEGER, ALLOCATABLE :: RNR_IM(:), CNR_IM(:), SDNR_IM(:)

  INTEGER, ALLOCATABLE :: Savenze_im(:)
  TYPE(SaveDPRow), ALLOCATABLE :: SaveInterpolationMatrix(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveRNR_IM(:), SaveCNR_IM(:), SaveSDNR_IM(:)


  ! The BEM matrix for finding the scalar potential at the surface of the
  ! material with physical boundary constraints (ie phi = 0 at infinity)
  ! from the scalar potential with neumann constraints
  ! (ie grad(phi) . n = 0 at the material surface.
  REAL(KIND=DP), ALLOCATABLE :: BA(:,:)
  INTEGER, ALLOCATABLE :: BoundaryNodeIndex(:)

  TYPE(SaveDPArray), ALLOCATABLE :: SaveBA(:)
  TYPE(SaveIntRow), ALLOCATABLE :: SaveBoundaryNodeIndex(:)




  ! PNM_RWORK, PNM_IWORK, PDM_RWORK, PDM_IWORK dependant on
  ! PoissonNeumannMatrix, PoissonDirichletMatrix.
  ! These hold the partial Cholesky decompositions for
  ! PoissonNeumannMatrix and PoissonDirichletMatrix as calculated in
  ! DSICCG for use in DCG, but cached for repeated use.
  REAL(KIND=DP), ALLOCATABLE :: PNM_RWORK(:), PDM_RWORK(:)
  INTEGER,       ALLOCATABLE :: PNM_IWORK(:), PDM_IWORK(:)

  TYPE(SaveDPRow),  ALLOCATABLE :: SavePNM_RWORK(:), SavePDM_RWORK(:)
  TYPE(SaveINTRow), ALLOCATABLE :: SavePNM_IWORK(:), SavePDM_IWORK(:)


  CONTAINS


  !> Initialize the variables of the finite_element module.
  SUBROUTINE InitializeFiniteElement()

    CALL DestroyFiniteElement()

    FEMTolerance = 1.d-10
  END SUBROUTINE InitializeFiniteElement


  !> Destroy and clean up the variables in the finite_element module.
  SUBROUTINE DestroyFiniteElement()
    INTERFACE TryDeallocate
      PROCEDURE :: &
        TryDeallocateDP, TryDeallocateDPArr, &
        TryDeallocateInt
    END INTERFACE

    IF(ALLOCATED(PoissonNeumannMatrix)) DEALLOCATE(PoissonNeumannMatrix)
    IF(ALLOCATED(RNR_PNM)) DEALLOCATE(RNR_PNM)
    IF(ALLOCATED(CNR_PNM)) DEALLOCATE(CNR_PNM)

    IF(ALLOCATED(Savenze_pnm)) DEALLOCATE(Savenze_pnm)
    CALL TryDeallocate(SavePoissonNeumannMatrix)
    CALL TryDeallocate(SaveRNR_PNM)
    CALL TryDeallocate(SaveCNR_PNM)


    IF(ALLOCATED(PoissonDirichletMatrix)) DEALLOCATE(PoissonDirichletMatrix)
    IF(ALLOCATED(RNR_PDM)) DEALLOCATE(RNR_PDM)
    IF(ALLOCATED(CNR_PDM)) DEALLOCATE(CNR_PDM)

    IF(ALLOCATED(Savenze_pdm)) DEALLOCATE(Savenze_pdm)
    CALL TryDeallocate(SavePoissonDirichletMatrix)
    CALL TryDeallocate(SaveRNR_PDM)
    CALL TryDeallocate(SaveCNR_PDM)


    IF(ALLOCATED(ExchangeMatrix)) DEALLOCATE(ExchangeMatrix)
    IF(ALLOCATED(RNR_EM))  DEALLOCATE(RNR_EM)
    IF(ALLOCATED(CNR_EM))  DEALLOCATE(CNR_EM)
    IF(ALLOCATED(SDNR_EM)) DEALLOCATE(SDNR_EM)

    IF(ALLOCATED(Savenze_em)) DEALLOCATE(Savenze_em)
    CALL TryDeallocate(SaveExchangeMatrix)
    CALL TryDeallocate(SaveRNR_EM)
    CALL TryDeallocate(SaveCNR_EM)
    CALL TryDeallocate(SaveSDNR_EM)


    IF(ALLOCATED(YA4))   DEALLOCATE(YA4)
    IF(ALLOCATED(FAX))   DEALLOCATE(FAX)
    IF(ALLOCATED(FAY))   DEALLOCATE(FAY)
    IF(ALLOCATED(FAZ))   DEALLOCATE(FAZ)
    IF(ALLOCATED(RNR4))  DEALLOCATE(RNR4)
    IF(ALLOCATED(CNR4))  DEALLOCATE(CNR4)
    IF(ALLOCATED(SDNR4)) DEALLOCATE(SDNR4)

    IF(ALLOCATED(Savenze4)) DEALLOCATE(Savenze4)
    CALL TryDeallocate(SaveYA4)
    CALL TryDeallocate(SaveFAX)
    CALL TryDeallocate(SaveFAY)
    CALL TryDeallocate(SaveFAZ)
    CALL TryDeallocate(SaveRNR4)
    CALL TryDeallocate(SaveCNR4)
    CALL TryDeallocate(SaveSDNR4)


    IF(ALLOCATED(InterpolationMatrix)) DEALLOCATE(InterpolationMatrix)
    IF(ALLOCATED(RNR_IM))  DEALLOCATE(RNR_IM)
    IF(ALLOCATED(CNR_IM))  DEALLOCATE(CNR_IM)
    IF(ALLOCATED(SDNR_IM)) DEALLOCATE(SDNR_IM)


    IF(ALLOCATED(BA)) DEALLOCATE(BA)
    IF(ALLOCATED(BoundaryNodeIndex)) DEALLOCATE(BoundaryNodeIndex)

    CALL TryDeallocate(SaveBA)
    CALL TryDeallocate(SaveBoundaryNodeIndex)


    IF(ALLOCATED(PNM_RWORK)) DEALLOCATE(PNM_RWORK)
    IF(ALLOCATED(PDM_RWORK)) DEALLOCATE(PDM_RWORK)
    IF(ALLOCATED(PNM_IWORK)) DEALLOCATE(PNM_IWORK)
    IF(ALLOCATED(PDM_IWORK)) DEALLOCATE(PDM_IWORK)

    CALL TryDeallocate(SavePNM_RWORK)
    CALL TryDeallocate(SavePDM_RWORK)
    CALL TryDeallocate(SavePNM_IWORK)
    CALL TryDeallocate(SavePDM_IWORK)

  CONTAINS
    SUBROUTINE TryDeallocateDP(s)
      TYPE(SaveDPRow), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%DPSave)) DEALLOCATE(s(i)%DPSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateDP

    SUBROUTINE TryDeallocateInt(s)
      TYPE(SaveIntRow), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%IntSave)) DEALLOCATE(s(i)%IntSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateInt

    SUBROUTINE TryDeallocateDPArr(s)
      TYPE(SaveDPArray), ALLOCATABLE, INTENT(INOUT) :: s(:)
      INTEGER :: i
      IF(ALLOCATED(s)) THEN
        DO i=1,SIZE(s)
          IF(ALLOCATED(s(i)%DPArrSave)) DEALLOCATE(s(i)%DPArrSave)
        END DO
        DEALLOCATE(s)
      END IF
    END SUBROUTINE TryDeallocateDPArr
  END SUBROUTINE DestroyFiniteElement


  !> Build the FEM representations
  SUBROUTINE BuildFiniteElement()
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    ! kstiff, kstiff2 : Contributions to the Geometric stiffness
    !   matrices for finite elements on the mesh
    ! kinterpolatestiff : Contributions to the interpolation stiffness
    !   matrices on the mesh
    REAL(KIND=DP), ALLOCATABLE :: kstiff(:), kstiff2(:,:)
    REAL(KIND=DP), ALLOCATABLE :: kinterpolatestiff(:)

    ! krow,kcol  : Indices on sparse stiffness matrix contributions
    ! kpid       : Tet Property ID on sparse stiffness matrix contributions
    ! sparse     : Sorted indices for SLAP sparse matrix contributions
    INTEGER, ALLOCATABLE  :: krow(:), kcol(:), kpid(:), sparse(:)

    ALLOCATE(kstiff(16*NTRI), kstiff2(16*NTRI,3), kinterpolatestiff(16*NTRI))
    ALLOCATE(krow(16*NTRI), kcol(16*NTRI), kpid(16*NTRI), sparse(16*NTRI))


    ! BEM Energy calcualtor
    CALL DemagStiff( &
      TIL, TetSubDomains, b, c, d, vol, TetSolid, solid, &
      kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
    )
    CALL NonZeroStiff( &
      NNODE, NodeOnBoundary, &
      kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
    )
    DEALLOCATE(kstiff, kstiff2, kinterpolatestiff)
    DEALLOCATE(krow, kcol, kpid, sparse)

    CALL ForceMat()
    CALL BoundMata()
  END SUBROUTINE BuildFiniteElement


  !---------------------------------------------------------------
  !          StiffMatrixAllocate and SparseMatrixAllocate
  !---------------------------------------------------------------

  !> Allocate the FEM sparse matrices
  !>
  !> @param[in] nze_pnm  The number of non-zero entries for the Poisson matrix
  !>                     with Neumann boundary conditions (PNM).
  !> @param[in] ncol_pnm The number of columns in the PNM matrix.
  !> @param[in] nze_pdm  The number of non-zero entries for the Poisson matrix
  !>                     with Dirichlet bounary conditions (PDM).
  !> @param[in] ncol_pdn The number of columns in the PDM matrix.
  !> @param[in] nze4     The number of non-zero entries in the RHS vector of
  !>                     the micromagnetic equations, represented by YA4.
  !> @param[in] ncol4    The number of columns in the YA4 vector.
  !> @param[in] nze_im   The number of non-zero entries in the multiphase
  !>                     interpolation matrix (IM).
  !> @param[in] ncol_im  The number of columns in the IM matrix.
  !> @param[in] nbnode   The number of nodes on the boundary of the magnetic
  !>                     region.
  SUBROUTINE SparseMatrixAllocate( &
    nze_pnm, ncol_pnm, &
    nze_pdm, ncol_pdm, &
    nze_em, ncol_em, &
    nze4, ncol4, &
    nze_im, ncol_im, &
    nbnode &
  )
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nze_pnm, ncol_pnm
    INTEGER, INTENT(IN) :: nze_pdm, ncol_pdm
    INTEGER, INTENT(IN) :: nze_em, ncol_em
    INTEGER, INTENT(IN) :: nze4, ncol4
    INTEGER, INTENT(IN) :: nze_im, ncol_im
    INTEGER, INTENT(IN) :: nbnode

    IF(ALLOCATED(PoissonNeumannMatrix))   DEALLOCATE(PoissonNeumannMatrix)
    IF(ALLOCATED(RNR_PNM)) DEALLOCATE(RNR_PNM)
    IF(ALLOCATED(CNR_PNM)) DEALLOCATE(CNR_PNM)
    IF(ALLOCATED(PNM_RWORK))  DEALLOCATE(PNM_RWORK)
    IF(ALLOCATED(PNM_IWORK))  DEALLOCATE(PNM_IWORK)

    IF(ALLOCATED(PoissonDirichletMatrix)) DEALLOCATE(PoissonDirichletMatrix)
    IF(ALLOCATED(RNR_PDM)) DEALLOCATE(RNR_PDM)
    IF(ALLOCATED(CNR_PDM)) DEALLOCATE(CNR_PDM)
    IF(ALLOCATED(PDM_RWORK)) DEALLOCATE(PDM_RWORK)
    IF(ALLOCATED(PDM_IWORK)) DEALLOCATE(PDM_IWORK)

    IF(ALLOCATED(ExchangeMatrix)) DEALLOCATE(ExchangeMatrix)
    IF(ALLOCATED(RNR_EM))  DEALLOCATE(RNR_EM)
    IF(ALLOCATED(CNR_EM))  DEALLOCATE(CNR_EM)
    IF(ALLOCATED(SDNR_EM)) DEALLOCATE(SDNR_EM)

    IF(ALLOCATED(YA4))   DEALLOCATE(YA4)
    IF(ALLOCATED(FAX))   DEALLOCATE(FAX)
    IF(ALLOCATED(FAY))   DEALLOCATE(FAY)
    IF(ALLOCATED(FAZ))   DEALLOCATE(FAZ)
    IF(ALLOCATED(RNR4))  DEALLOCATE(RNR4)
    IF(ALLOCATED(CNR4))  DEALLOCATE(CNR4)
    IF(ALLOCATED(SDNR4)) DEALLOCATE(SDNR4)

    IF(ALLOCATED(InterpolationMatrix)) DEALLOCATE(InterpolationMatrix)
    IF(ALLOCATED(RNR_IM))  DEALLOCATE(RNR_IM)
    IF(ALLOCATED(CNR_IM))  DEALLOCATE(CNR_IM)
    IF(ALLOCATED(SDNR_IM)) DEALLOCATE(SDNR_IM)

    IF(ALLOCATED(BA)) DEALLOCATE(BA)
    IF(ALLOCATED(BoundaryNodeIndex)) DEALLOCATE(BoundaryNodeIndex)

    ALLOCATE( &
      PoissonNeumannMatrix(nze_pnm), &
      RNR_PNM(nze_pnm), CNR_PNM(ncol_pnm+1), &
      PNM_RWORK(nze_pnm + 5*ncol_pnm), PNM_IWORK(nze_pnm + ncol_pnm + 11), &
    )

    ALLOCATE( &
      PoissonDirichletMatrix(nze_pdm), &
      RNR_PDM(nze_pdm), CNR_PDM(ncol_pdm+1), &
      PDM_RWORK(nze_pdm + 5*ncol_pdm), PDM_IWORK(nze_pdm + ncol_pdm + 11) &
    )

    ALLOCATE( &
      ExchangeMatrix(nze_em), &
      RNR_EM(nze_em), CNR_EM(ncol_em+1), &
      SDNR_EM(nze_em) &
    )

    ALLOCATE( &
      YA4(nze4,3), FAX(nze4), FAY(nze4), FAZ(nze4), &
      RNR4(nze4), CNR4(ncol4+1), &
      SDNR4(nze4) &
    )

    ALLOCATE( &
      InterpolationMatrix(nze_im), &
      RNR_IM(nze_im), CNR_IM(ncol_im+1), &
      SDNR_IM(nze_im) &
    )

    ALLOCATE(BA(nbnode,nbnode), BoundaryNodeIndex(nbnode))

  END SUBROUTINE SparseMatrixAllocate


  !---------------------------------------------------------------
  ! BEM  : DEMAGSTIFF  ===  Purely geometric
  !---------------------------------------------------------------

  !> Generate the entries for the FEM matrices
  SUBROUTINE DemagStiff( &
    TIL, TetSubDomains, b, c, d, vol, TetSolid, NodeSolid, &
    kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
  )
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: TIL(:,:), TetSubDomains(:)
    REAL(KIND=DP), INTENT(IN) :: b(:,:), c(:,:), d(:,:), vol(:)
    REAL(KIND=DP), INTENT(IN) :: TetSolid(:,:), NodeSolid(:)

    REAL(KIND=DP), INTENT(OUT) :: kstiff(:), kstiff2(:,:), kinterpolatestiff(:)
    INTEGER, INTENT(OUT) :: krow(:), kcol(:), kpid(:), sparse(:)

    INTEGER :: knc, i, j, k
    INTEGER, ALLOCATABLE :: colrowind(:,:)

    knc = 0

    ALLOCATE(colrowind(16*SIZE(TIL,1),4))

    WRITE(*,*)
    WRITE(*,*) "Building components for:"
    WRITE(*,*) "I grad(phi) . grad(v) dV = I m . grad(v) dV + I v * m . dS"
    WRITE(*,*) "^^^^^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^"

    kstiff  = 0
    kstiff2 = 0
    kinterpolatestiff = 0

    DO i=1,SIZE(TIL,1)

      DO j=1,4
        DO k=1,4
          knc = 16*(i-1) + 4*(j-1) + k

          ! integral_{tet_i} grad(phi_j) . grad(v_k) dV
          ! where phi,v are linear elements
          kstiff(knc) = (b(i,j)*b(i,k)+c(i,j)*c(i,k)+d(i,j)*d(i,k)) &
                        /(36.d0*vol(i))

          ! m(j,:) * kstiff(knc,:) = integral_{tet_i} m(j,:) . grad(v_k)
          ! where m,v are linear elements
          kstiff2(knc,1) = b(i,j)/(24.d0)
          kstiff2(knc,2) = c(i,j)/(24.d0)
          kstiff2(knc,3) = d(i,j)/(24.d0)

          ! Track column row and material id of entry knc
          krow(knc) = TIL(i,j)
          kcol(knc) = TIL(i,k)
          kpid(knc) = TetSubDomains(i)

          ! Store them in colrowind for later sorting, in order of sorting
          ! sorting importance. i.e. column-row, then material, then
          ! in order of knc. The values knc will then be stored in this order.
          colrowind(knc,1) = TIL(i,k)
          colrowind(knc,2) = TIL(i,j)
          colrowind(knc,3) = TetSubDomains(i)
          colrowind(knc,4) = knc

          ! Interpolation of subdomains given by splitting region up into
          ! vbox for each subdomain.
          IF(j .EQ. k) THEN
            kinterpolatestiff(knc) = vol(i) / 4
          ENDIF
        END DO
      END DO

    END DO

    WRITE(*,*) "-- Done"

    WRITE(*,*)
    WRITE(*,*) "Sorting built components for Sparse Matrix"


    !
    ! Sparse matrices in SLAP column FORMAT (for GMRES)
    !
    ! We want to sort the entries in increasing order of
    !   (column, row, subdomain, tet_id)
    ! We also want diagonal values at the front of each new column.
    ! eg for a 3x3 matrix
    !   (1, 1, ...)
    !   (1, 2, ...)
    !   (1, 3, ...)
    !   (2, 2, ...)
    !   (2, 1, ...)
    !   (2, 3, ...)
    !   (3, 3, ...)
    !   (3, 1, ...)
    !   (3, 2, ...)
    ! we'll achieve this by temporary setting the row value for diagonal
    ! entries to 0.
    ! There's no need to set it back, since sparse(:) will be set from
    ! colrowind(:,4), and colrowind will never be used again.
    !

    ! Set diagonal row values to 0
    DO i=1,knc
      IF(colrowind(i,1) .EQ. colrowind(i,2)) THEN
        colrowind(i,2) = 0
      END IF
    END DO

    ! do the actual sorting
    CALL qsort(colrowind, default_smaller)

    ! Store the sorted values of colrowind in the array sparse(:)
    sparse(:) = colrowind(:,4)


    WRITE(*,*) "-- Done"

  END SUBROUTINE DemagStiff


  !---------------------------------------------------------------
  ! BEM  : NONZEROSTIFF   purely geometric
  !---------------------------------------------------------------

  !> Assemble the FEM values generated by demagstiff into the sparse
  !> FEM matrices.
  !>
  !> @param[in] NNODE   The number of nodes, i.e. the number of
  !>                    columns.
  !> @param[in] is_boundary_node An array of values which are .TRUE. at index
  !>                             `i` if vertex `i` of `VCL` is on the boundary.
  !> @param[in] kstiff  An array containing the values for the Poisson FEM
  !>                    matrices.
  !> @param[in] kstiff2 An array containing the values for the Exchange FEM
  !>                    matrix.
  !> @param[in] kinterpolatestiff An array containing the values for the
  !>                              multiphase interpolation matrix.
  !> @param[in] krow An array indexing the FEM matrix row for the given sparse
  !>                 index.
  !> @param[in] kcol An array indexing the FEM matrix column for the given
  !>                 sparse index.
  !> @param[in] sparse The sorted indices of the sparce matrix entries.
  SUBROUTINE NonZeroStiff( &
    NNODE, is_boundary_node, &
    kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
  )
    USE slatec_dsics
    USE slatec_xermsg
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: NNODE
    LOGICAL, INTENT(IN) :: is_boundary_node(:)
    REAL(KIND=DP), INTENT(IN) :: kstiff(:), kstiff2(:,:), kinterpolatestiff(:)
    INTEGER, INTENT(IN) :: krow(:), kcol(:), kpid(:), sparse(:)

    INTEGER :: i, j, j4, j_em, j_im, l

    INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
    INTEGER :: PNM_LOCJEL, PNM_LOCIEL, PNM_LOCIW
    INTEGER :: PDM_LOCJEL, PDM_LOCIEL, PDM_LOCIW
    INTEGER :: PNM_LOCEL, PNM_LOCDIN, PNM_LOCR, PNM_LOCZ, PNM_LOCP, PNM_LOCDZ, &
      PNM_LOCW
    INTEGER :: PDM_LOCEL, PDM_LOCDIN, PDM_LOCR, PDM_LOCZ, PDM_LOCP, &
      PDM_LOCDZ, PDM_LOCW
    INTEGER :: IERR
    CHARACTER(len=8) :: XERN1
    LOGICAL :: new_row, new_column, new_subdomain, on_boundary, on_diagonal

    INTEGER :: nbnode


    ! count # of actual non zero entries, nze_pnm for PoissonNeumannMatrix
    ! count # of actual non zero entries, nze_pdm for PoissonDirichletMatrix
    ! count # of actual non zero entries, nze_pdm for ExchangeMatrix
    ! count # of actual non zero entries, nze4 for YA4, FAX, FAY, FAZ

    nze_pnm = 0
    nze_pdm = 0
    nze_em  = 0
    nze4    = 0
    nze_im  = 0

    DO i=1,SIZE(sparse)
      IF(i .GT. 1) THEN
        new_column    = kcol(sparse(i)) .NE. kcol(sparse(i-1))
        new_row       = krow(sparse(i)) .NE. krow(sparse(i-1))
        new_subdomain = kpid(sparse(i)) .NE. kpid(sparse(i-1))
      ELSE
        new_column    = .TRUE.
        new_row       = .TRUE.
        new_subdomain = .TRUE.
      END IF
      on_boundary = &
        is_boundary_node(kcol(sparse(i))) &
        .OR. &
        is_boundary_node(krow(sparse(i)))
      on_diagonal = krow(sparse(i)) .EQ. kcol(sparse(i))

      ! If we've moved on to a new matrix entry, add to nze_pnm
      IF(new_column .OR. new_row) THEN
        nze_pnm = nze_pnm + 1
      END IF

      ! If we've moved to a new matrix entry, and it's not on the boundary
      ! or it is a diagonal, add to nze_pdm
      IF( &
        (new_column .OR. new_row) &
        .AND. &
        ((.NOT. on_boundary) .OR. on_diagonal) &
      ) THEN
        nze_pdm = nze_pdm + 1
      END IF

      ! If we've moved on to a new matrix entry, or a new subdomain,
      ! add to nze_em and nze4
      IF(new_column .OR. new_row .OR. new_subdomain) THEN
        nze_em = nze_em + 1
        nze4   = nze4 + 1
      END IF

      ! If we've moved to a new matrix entry, or a new subdomain, and
      ! we're on the diagonal, add to nze_im
      IF( &
        (new_column .OR. new_row .OR. new_subdomain) &
        .AND. &
        on_diagonal &
      ) THEN
        nze_im = nze_im + 1
      END IF

    END DO

    nbnode = 0
    DO i=1,SIZE(is_boundary_node)
      IF(is_boundary_node(i)) nbnode = nbnode + 1
    END DO


    ! Allocate the sparse arrays
    call SparseMatrixAllocate( &
      nze_pnm, NNODE, &
      nze_pdm, NNODE, &
      nze_em, NNODE, &
      nze4, NNODE, &
      nze_im, NNODE, &
      nbnode &
    )


    !
    ! Build the sparse arrays
    !

    WRITE(*,*)
    WRITE(*,*) "Building Sparse Matrix for Poisson Eqn with Neumann BCs"

    PoissonNeumannMatrix = 0
    PoissonDirichletMatrix = 0
    ExchangeMatrix = 0
    YA4 = 0
    InterpolationMatrix = 0

    j  = 0
    j4 = 0
    j_em = 0
    j_im = 0
    l  = 0

    DO i=1,SIZE(sparse)

      ! Check index changes
      IF(i .GT. 1) THEN
        new_row       = krow(sparse(i)) .NE. krow(sparse(i-1))
        new_column    = kcol(sparse(i)) .NE. kcol(sparse(i-1))
        new_subdomain = kpid(sparse(i)) .NE. kpid(sparse(i-1))
      ELSE
        new_row       = .TRUE.
        new_column    = .TRUE.
        new_subdomain = .TRUE.
      END IF
      on_diagonal = krow(sparse(i)) .EQ. kcol(sparse(i))

      ! If we have any index changes...
      IF(new_row .OR. new_column .OR. new_subdomain) THEN

        ! If we're on a new column, tick CNR_PNM along
        IF(new_column) THEN
          l=l+1

          CNR_PNM(l) = j  + 1
          CNR_EM(l)  = j_em + 1
          CNR4(l)    = j4 + 1
          CNR_IM(l)  = j_im + 1
        END IF

        ! PNM tracks rows and cols, ignores material.
        !
        ! On row/column change, tick data pos along and track row number.
        IF(new_row .OR. new_column) THEN
          j = j+1

          RNR_PNM(j) = krow(sparse(i))
        END IF

        ! EM and YA4 track rows, cols and subdomains.
        !
        ! On row/column/material change, tick data pos along and track row
        ! number and subdomain number.
        IF(new_row .OR. new_column .OR. new_subdomain) THEN
          j4   = j4 + 1
          j_em = j_em + 1

          RNR_EM(j_em)  = krow(sparse(i))
          SDNR_EM(j_em) = kpid(sparse(i))

          RNR4(j4)  = krow(sparse(i))
          SDNR4(j4) = kpid(sparse(i))
        END IF

        ! IM tracks diagonal rows and cols, and tracks subdomains.
        !
        ! On any change, if we're on a diagonal, tick data pos along,
        ! track row number and subdomain number.
        IF((new_row .OR. new_column .OR. new_subdomain) .AND. on_diagonal) THEN
          j_im = j_im + 1

          RNR_IM(j_im)  = kcol(sparse(i))
          SDNR_IM(j_im) = kpid(sparse(i))
        END IF
      END IF

      PoissonNeumannMatrix(j) = PoissonNeumannMatrix(j) + kstiff(sparse(i))
      ExchangeMatrix(j_em)    = ExchangeMatrix(j_em)    + kstiff(sparse(i))
      YA4(j4,:) = YA4(j4,:) + kstiff2(sparse(i), :)
      IF(on_diagonal) THEN
        InterpolationMatrix(j_im) = InterpolationMatrix(j_im) &
          + kinterpolatestiff(sparse(i))
      END IF
    END DO

    CNR_PNM(NNODE+1) = nze_pnm + 1
    CNR_EM(NNODE+1)  = nze_em  + 1
    CNR4(NNODE+1)    = nze4 + 1
    CNR_IM(NNODE+1)  = nze_im + 1

    WRITE(*,*) "-- Done"

    ! form PoissonDirichletMatrix which is a duplicate of PoissonNeumannMatrix
    ! with boundary nodes set to 1 for phi2 set exterior boundary values to 0
    ! This corresponds to pure Dirichlet boundary conditions.

    WRITE(*,*)
    WRITE(*,*) "Building Sparse Matrix for Poisson Eqn with Dirichlet BCs"

    j=1
    DO i=1,NNODE

      CNR_PDM(i)=j

      DO l=CNR_PNM(i),CNR_PNM(i+1)-1
        ! Set diagonal to 1 for boundary row and column
        IF (is_boundary_node(RNR_PNM(l)) .AND. (RNR_PNM(l)==i) ) THEN
          PoissonDirichletMatrix(j)=1.0d0
          RNR_PDM(j)=i
          j=j+1
        ENDIF

        ! Only add non-boundary entries of PoissonNeumannMatrix
        IF ( &
            (.NOT. is_boundary_node(RNR_PNM(l))) &
            .AND. &
            (.NOT. is_boundary_node(i))  &
        ) THEN
          PoissonDirichletMatrix(j) = PoissonNeumannMatrix(l)
          RNR_PDM(j) = RNR_PNM(l)
          j=j+1
        ENDIF
      END DO

    END DO

    CNR_PDM(NNODE+1) = nze_pdm+1

    WRITE(*,*) "-- Done"


    ! Adding boundary condition to PoissonNeumannMatrix on the final node
    ! to break its singularity.
    ! During solving, this will node will be set to zero to avoid having
    ! to add the values to the RHS vector.
    DO i=1,NNODE
      DO j=CNR_PNM(i),CNR_PNM(i+1)-1
        IF(i .EQ. NNODE .OR. RNR_PNM(j) .EQ. NNODE) THEN
          IF(i .EQ. RNR_PNM(j)) THEN
            ! 1 on the diagonal
            PoissonNeumannMatrix(j) = 1
          ELSE
            ! 0 off the diagonal
            PoissonNeumannMatrix(j) = 0
          END IF
        END IF
      END DO
    END DO


    WRITE(*,*)
    WRITE(*,*) nze_pnm,  'non-zero entries in sparse matrix for Phi1'
    WRITE(*,*) nze_pdm, 'non-zero entries in sparse matrix for Phi2'
    WRITE(*,*) nze_em, 'non-zero entries in sparse matrix for Exchange'
    WRITE(*,*) nze4, 'non-zero entries in sparse matrix for RHS'


    !
    ! From DSICCG
    !

    ! Compute offsets for RWORK and IWORK for PoissonNeumannMatrix and
    ! PoissonDirichletMatrix for use with DSICS. This is lifted straight from
    ! DSICCS with values changed to suit the dummy arguments.

    PNM_LOCJEL = LOCIB
    PDM_LOCJEL = LOCIB
    PNM_LOCIEL = PNM_LOCJEL + nze_pnm
    PDM_LOCIEL = PDM_LOCJEL + nze_pdm
    PNM_LOCIW  = PNM_LOCIEL + NNODE + 1
    PDM_LOCIW  = PDM_LOCIEL + NNODE + 1

    PNM_LOCEL = LOCRB
    PDM_LOCEL = LOCRB
    PNM_LOCDIN = PNM_LOCEL + nze_pnm
    PDM_LOCDIN = PDM_LOCEL + nze_pdm
    PNM_LOCR = PNM_LOCDIN + NNODE
    PDM_LOCR = PDM_LOCDIN + NNODE
    PNM_LOCZ = PNM_LOCR + NNODE
    PDM_LOCZ = PDM_LOCR + NNODE
    PNM_LOCP = PNM_LOCZ + NNODE
    PDM_LOCP = PDM_LOCZ + NNODE
    PNM_LOCDZ = PNM_LOCP + NNODE
    PDM_LOCDZ = PDM_LOCP + NNODE
    PNM_LOCW = PNM_LOCDZ + NNODE
    PDM_LOCW = PDM_LOCDZ + NNODE

    ! IWORK(1) = NL
    PNM_IWORK(1) = nze_pnm
    PDM_IWORK(1) = nze_pdm
    ! IWORK(2) = LOCJEL
    PNM_IWORK(2) = PNM_LOCJEL
    PDM_IWORK(2) = PDM_LOCJEL
    ! IWORK(3) = LOCIEL
    PNM_IWORK(3) = PNM_LOCIEL
    PDM_IWORK(3) = PDM_LOCIEL
    ! IWORK(4) = LOCEL
    PNM_IWORK(4) = PNM_LOCEL
    PDM_IWORK(4) = PDM_LOCEL
    ! IWORK(5) = LOCDIN
    PNM_IWORK(5) = PNM_LOCDIN
    PDM_IWORK(5) = PDM_LOCDIN
    ! IWORK(9) = LOCIW
    PNM_IWORK(9) = PNM_LOCIW
    PDM_IWORK(9) = PDM_LOCIW
    ! IWORK(10) = LOCW
    PNM_IWORK(10) = PNM_LOCW
    PDM_IWORK(10) = PDM_LOCW


    !
    ! Call DSICS for PoissonNeumannMatrix and PDM.
    !

    WRITE(*,*)
    WRITE(*,*) "Building Preconditioning Matrix for Neumann Poisson"

    ! Generate PNM_RWORK, PNM_IWORK for PoissonNeumannMatrix
    CALL DSICS( &
      NNODE, nze_pnm, RNR_PNM, CNR_PNM, PoissonNeumannMatrix, 0, &
      (nze_pnm + NNODE)/2, PNM_IWORK(PNM_LOCIEL), PNM_IWORK(PNM_LOCJEL), &
      PNM_RWORK(PNM_LOCEL), PNM_RWORK(PNM_LOCDIN), PNM_RWORK(PNM_LOCR), IERR &
    )

    IF( IERR.NE.0 ) THEN
       WRITE (XERN1, '(I8)') IERR
       CALL XERMSG ('SLATEC', 'DSICCG', &
          'IC factorization broke down on step ' // XERN1 // &
          '.  Diagonal was set to unity and factorization proceeded.', &
          1, 1)
       IERR = 7
    ENDIF

    WRITE(*,*) "-- Done"

    WRITE(*,*)
    WRITE(*,*) "Building Preconditioning Matrix for Dirichlet Poisson"

    ! Generate PDM_RWORK, PDM_IWORK for PoissonDirichletMatrix
    CALL DSICS( &
      NNODE, nze_pdm, RNR_PDM, CNR_PDM, PoissonDirichletMatrix, 0, &
      (nze_pdm + NNODE)/2, PDM_IWORK(PDM_LOCIEL), PDM_IWORK(PDM_LOCJEL), &
      PDM_RWORK(PDM_LOCEL), PDM_RWORK(PDM_LOCDIN), PDM_RWORK(PDM_LOCR), IERR &
    )

    IF( IERR.NE.0 ) THEN
       WRITE (XERN1, '(I8)') IERR
       CALL XERMSG ('SLATEC', 'DSICCG', &
          'IC factorization broke down on step ' // XERN1 // &
          '.  Diagonal was set to unity and factorization proceeded.', &
          1, 1)
       IERR = 7
    ENDIF

    WRITE(*,*) "-- Done"

  END SUBROUTINE NonZeroStiff


  !---------------------------------------------------------------
  ! BEM  : FORCEMAT   geometric (boundary conditions)
  !---------------------------------------------------------------

  SUBROUTINE ForceMat()
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    INTEGER :: j,k,l,el,opp,nj,nk
    INTEGER :: idx(3)
    REAL(KIND=DP) :: BAX, BAY, BAZ



    WRITE(*,*)
    WRITE(*,*) "Building components for:"
    WRITE(*,*) "I grad(phi) . grad(v) dV = I m . grad(v) dV + I v * m . dS"
    WRITE(*,*) "                                              ^^^^^^^^^^^^"

    ! Form force vector matrices, FAX,FAY,FAZ
    ! FAX(:)*m(:,1) = - integral m_x * v * n_x dS
    FAX(:)=-YA4(:,1)
    FAY(:)=-YA4(:,2)
    FAZ(:)=-YA4(:,3)

    ! Add m.n Neumann condition to force vector
    ! Build up element contributions

    !DO i=1,BFCE
    DO el=1,NTRI
    DO opp=1,4

      ! Oriented area |A|*n(:) of triangle TIL(el, (/i,j,l/)) with
      ! n(:) the unit normal is given by the shape coefficients for
      ! the opposite node
      ! (1/2) (/ b(el, opp), c(el, opp), d(el, opp) /)

      ! add only if triangle is on the boundary, or on the boundary of
      ! a subdomain.
      ! MAX(NEIGH(el,opp),1) here to avoid evaluation of TetSubDomains(0) when
      ! NEIGH(el,opp) is 0.

      IF( &
        NEIGH(el,opp) .EQ. 0 &
        .OR. &
        TetSubDomains(el) .NE. TetSubDomains(MAX(NEIGH(el,opp),1)) &
      ) THEN

        ! Set j,k,l as properly oriented triangle
        SELECT CASE (opp)
          CASE(1)
            j = 2
            k = 3
            l = 4
          CASE(2)
            j = 3
            k = 1
            l = 4
          CASE(3)
            j = 4
            k = 1
            l = 2
          !CASE(4)
          CASE DEFAULT
            j = 2
            k = 1
            l = 3
        END SELECT

        idx(1)=TIL(el,j)
        idx(2)=TIL(el,k)
        idx(3)=TIL(el,l)

        ! BAX = |A|*n(1) / 12 * (if brow != bcol: 1; else: 2)
        !     = (1/2) b(el,opp) / 12 * (...)
        DO nj=1,3
        DO nk=1,3

          ! Find the right point in FAX to add it
          l=idx(nj)
          k=idx(nk)
          DO j=CNR4(l),CNR4(l+1)-1
            IF (RNR4(j)==k .AND. SDNR4(j) .EQ. TetSubDomains(el)) THEN
              ! Add to FAX where idx(nk) refers to the v(idx(nk)) row

              ! Find contribution from triangle el due to nodes idx(nj) and
              ! idx(nk)
              BAX=-b(el,opp)/24.d0
              BAY=-c(el,opp)/24.d0
              BAZ=-d(el,opp)/24.d0
              IF(nj .EQ. nk) THEN
                BAX = 2*BAX
                BAY = 2*BAY
                BAZ = 2*BAZ
              END IF

              FAX(j)=FAX(j)+BAX
              FAY(j)=FAY(j)+BAY
              FAZ(j)=FAZ(j)+BAZ
            ENDIF
          END DO
        END DO
        END DO
      END IF

    END DO
    END DO


    WRITE(*,*) "-- Done"
  END SUBROUTINE ForceMat


  !---------------------------------------------------------------
  ! BEM  : BOUNDMATA
  !---------------------------------------------------------------

  SUBROUTINE boundmata()
    USE Tetrahedral_Mesh_Data
    USE Utils

    IMPLICIT NONE

    REAL(KIND=DP) rx,ry,rz &
      & ,area, normb,normc,normd,normval,SAngle &
      & ,neta1,neta2,neta3,zeta,xp,xq,xr &
      & ,yp,yq,yr,zp,zq,zr,s1,s2,s3,rho1,rho2,rho3,gam11 &
      & ,gam12,gam13,gam22,gam21,gam23,gam33,gam31,gam32,q1,q2,q3 &
      & ,tempval(3)
    INTEGER i,j,el,opp   &
      & ,k,l,dum2,p1,p2,p3,cn,dum3,p,q,r,n(3),rc,rl  &
      & ,bk,bi

    INTEGER, ALLOCATABLE :: NodeToBoundary(:)


    ! Getting rid of compiler warnings
    l = 0
    k = 0
    j = 0

    ALLOCATE(NodeToBoundary(NNODE))


    NodeToBoundary = HUGE(NodeToBoundary)

    rl = 1
    DO i=1,NNODE
      IF(NodeOnBoundary(i)) THEN
        BoundaryNodeIndex(rl) = i
        NodeToBoundary(i) = rl

        rl = rl+1
      END IF
    END DO


    WRITE(*,*)
    WRITE(*,*) 'Assembling multiplicative matrix for phi2'
    WRITE(*,*) "Building components for:"
    WRITE(*,*) "phi2 = (LU-1)(phi1) on the boundary"
    WRITE(*,*) "       ^^^^^^"
    WRITE(*,*) 


    BA = 0


    ! Assemble entries for operator LU for G(r, VCL(k,:)) from
    ! D. Lindholm, “Three-dimensional magnetostatic fields from point-matched
    ! integral equations with linearly varying scalar sources,” IEEE
    ! Transactions on Magnetics, vol. 20, no. 5, pp. 2025–2032, 1984.

    ! Iterate over boundary nodes
    DO bk=1,BNODE
      k = BoundaryNodeIndex(bk)

      !      node k is the observation point
      rx=VCL(k,1)
      ry=VCL(k,2)
      rz=VCL(k,3)

      ! Calculate contributions to fx2
      ! Iterate over surface triangles
      DO dum3=1,BFCE

        el=BDFACE(dum3,1)
        opp=BDFACE(dum3,2)
        SELECT CASE (opp)
          CASE (1)
            p = 3
            q = 2
            r = 4

          CASE (2)
            p = 1
            q = 3
            r = 4

          CASE (3)
            p = 4
            q = 2
            r = 1

          !CASE (4)
          CASE DEFAULT
            p = 1
            q = 2
            r = 3
        END SELECT

        cn=k
        p3=TIL(el,p)
        p2=TIL(el,q)
        p1=TIL(el,r)

        xp=VCL(p1,1)
        xq=VCL(p2,1)
        xr=VCL(p3,1)
        yp=VCL(p1,2)
        yq=VCL(p2,2)
        yr=VCL(p3,2)
        zp=VCL(p1,3)
        zq=VCL(p2,3)
        zr=VCL(p3,3)

        ! get the solid anglemage by the element surface dum3 at the
        ! obervation point
        ! Lindholm's variable S_t

        SAngle=GET_ANGLE(cn,p1,p2,p3)
        ! IF the solid angle is ZERO THEN the observation point and the
        ! element dum3 must be in the same place, and the linhdolm funtion
        ! will be zero

        ! -- the INWARD normal to surface face of a boundary element
        !   (ie Lindholms variable Zeta)

        normval=sqrt(b(el,opp)**2+c(el,opp)**2+d(el,opp)**2)
        normb=-1.*(b(el,opp)/normval)
        normc=-1.*(c(el,opp)/normval)
        normd=-1.*(d(el,opp)/normval)

        area=0.5*normval

        s1=sqrt((xq-xp)**2+(yq-yp)**2+(zq-zp)**2)
        s2=sqrt((xr-xq)**2+(yr-yq)**2+(zr-zq)**2)
        s3=sqrt((xp-xr)**2+(yp-yr)**2+(zp-zr)**2)

        rho1=sqrt((xp-rx)**2+(yp-ry)**2+(zp-rz)**2)
        rho2=sqrt((xq-rx)**2+(yq-ry)**2+(zq-rz)**2)
        rho3=sqrt((xr-rx)**2+(yr-ry)**2+(zr-rz)**2)

        zeta=((normb)*(xp-rx)+(normc)*(yp-ry)+normd*(zp-rz))

        gam11=((xr-xq)*(xq-xp)+(yr-yq)*(yq-yp)+(zr-zq)*(zq-zp))/(s2*s1)
        gam21=((xp-xr)*(xq-xp)+(yp-yr)*(yq-yp)+(zp-zr)*(zq-zp))/(s3*s1)
        gam31=1.0d0
        gam12=1.0d0
        gam22=((xp-xr)*(xr-xq)+(yp-yr)*(yr-yq)+(zp-zr)*(zr-zq))/(s3*s2)
        gam32=gam11
        gam13=gam22
        gam23=1.0d0
        gam33=gam21

        neta1=((normc*(zq-zp)-normd*(yq-yp))*(xp-rx)+(normd*(xq-xp)-normb &
          &   *(zq-zp))*(yp-ry) + (normb*(yq-yp)-normc*(xq-xp))*(zp-rz) )/s1

        neta2=((normc*(zr-zq)-normd*(yr-yq))*(xq-rx)+(normd*(xr-xq)-normb &
          &   *(zr-zq))*(yq-ry)+(normb*(yr-yq)-normc*(xr-xq))*(zq-rz) )/s2

        neta3=((normc*(zp-zr)-normd*(yp-yr))*(xr-rx)+(normd*(xp-xr)-normb &
          &   *(zp-zr))*(yr-ry)+(normb*(yp-yr)-normc*(xp-xr))*(zr-rz) )/s3

        !       IF ((p1/=k).AND.(p2/=k).AND.(p3/=k)) THEN
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        IF ((p1==k).OR.(p2==k).OR.(p3==k)) THEN
          IF (NONZERO(SAngle)) THEN
            print*,'Solid = ', SAngle
          END IF
        ELSE
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          q1=log((rho1+rho2+s1)/(rho1+rho2-s1))
          q2=log((rho2+rho3+s2)/(rho2+rho3-s2))
          q3=log((rho3+rho1+s3)/(rho3+rho1-s3))

          dum2=int(DSIGN(1.0d0,zeta))

          n(1)=TIL(el,p)
          tempval(1) = &
            s2*( dum2*neta2*SAngle-zeta*(gam11*q1+gam12*q2+gam13*q3) ) &
              / (8*pi*area)

          n(2)=TIL(el,q)
          tempval(2) = &
            s3*(dum2*neta3*SAngle-zeta*(gam21*q1+gam22*q2+gam23*q3)) &
              / (8*pi*area)

          n(3)=TIL(el,r)
          tempval(3) = &
            s1*(dum2*neta1*SAngle-zeta*(gam31*q1+gam32*q2+gam33*q3)) &
              /(8*pi*area)

          BA(NodeToBoundary(n(:)), bk) = BA(NodeToBoundary(n(:)), bk) &
            + tempval(:)
        END IF

      END DO !loop over all boundary elements

      ! add on solid angle part
      BA(bk, bk) = BA(bk, bk) + ((solid(BoundaryNodeIndex(bk))/(4.*pi))-1.d0)

    END DO !bk

  END SUBROUTINE boundmata


  !---------------------------------------------------------------		
  !  SaveFEM(MeshNo) saves all important global variables
  !                   of the FEM into arrays
  !                   The index of the FEMState  is MeshNo
  !---------------------------------------------------------------

  SUBROUTINE SaveFEM(MeshNo)
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: MeshNo


    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(Savenze_pnm).EQV..FALSE.)  THEN

      ALLOCATE(Savenze_pnm(MaxMeshNumber))
      ALLOCATE(SavePoissonNeumannMatrix(MaxMeshNumber))
      ALLOCATE(SaveRNR_PNM(MaxMeshNumber))
      ALLOCATE(SaveCNR_PNM(MaxMeshNumber))
      ALLOCATE(SavePNM_RWORK(MaxMeshNumber))
      ALLOCATE(SavePNM_IWORK(MaxMeshNumber))

      ALLOCATE(Savenze_pdm(MaxMeshNumber))
      ALLOCATE(SavePoissonDirichletMatrix(MaxMeshNumber))
      ALLOCATE(SaveRNR_PDM(MaxMeshNumber))
      ALLOCATE(SaveCNR_PDM(MaxMeshNumber))
      ALLOCATE(SavePDM_RWORK(MaxMeshNumber))
      ALLOCATE(SavePDM_IWORK(MaxMeshNumber))

      ALLOCATE(Savenze_em(MaxMeshNumber))
      ALLOCATE(SaveExchangeMatrix(MaxMeshNumber))
      ALLOCATE(SaveRNR_EM(MaxMeshNumber))
      ALLOCATE(SaveCNR_EM(MaxMeshNumber))
      ALLOCATE(SaveSDNR_EM(MaxMeshNumber))

      ALLOCATE(Savenze4(MaxMeshNumber))
      ALLOCATE(SaveYA4(MaxMeshNumber))
      ALLOCATE(SaveFAX(MaxMeshNumber))
      ALLOCATE(SaveFAY(MaxMeshNumber))
      ALLOCATE(SaveFAZ(MaxMeshNumber))
      ALLOCATE(SaveRNR4(MaxMeshNumber))
      ALLOCATE(SaveCNR4(MaxMeshNumber))
      ALLOCATE(SaveSDNR4(MaxMeshNumber))

      ALLOCATE(Savenze_im(MaxMeshNumber))
      ALLOCATE(SaveInterpolationMatrix(MaxMeshNumber))
      ALLOCATE(SaveRNR_IM(MaxMeshNumber))
      ALLOCATE(SaveCNR_IM(MaxMeshNumber))
      ALLOCATE(SaveSDNR_IM(MaxMeshNumber))

      ALLOCATE(SaveBA(MaxMeshNumber))
      ALLOCATE(SaveBoundaryNodeIndex(MaxMeshNumber))
    ENDIF


    Savenze_pnm(MeshNo)=nze_pnm
    CALL AllocSet( &
      PoissonNeumannMatrix, SavePoissonNeumannMatrix(MeshNo)%DPSave &
    )
    CALL AllocSet(RNR_PNM, SaveRNR_PNM(MeshNo)%IntSave)
    CALL AllocSet(CNR_PNM, SaveCNR_PNM(MeshNo)%IntSave)
    CALL AllocSet(PNM_RWORK, SavePNM_RWORK(MeshNo)%DPSave)
    CALL AllocSet(PNM_IWORK, SavePNM_IWORK(MeshNo)%IntSave)

    Savenze_pdm(MeshNo)=nze_pdm
    CALL AllocSet( &
      PoissonDirichletMatrix, SavePoissonDirichletMatrix(MeshNo)%DPSave &
    )
    CALL AllocSet(RNR_PDM, SaveRNR_PDM(MeshNo)%IntSave)
    CALL AllocSet(CNR_PDM, SaveCNR_PDM(MeshNo)%IntSave)
    CALL AllocSet(PDM_RWORK, SavePDM_RWORK(MeshNo)%DPSave)
    CALL AllocSet(PDM_IWORK, SavePDM_IWORK(MeshNo)%IntSave)

    Savenze_em(MeshNo)=nze_em
    CALL AllocSet( &
      ExchangeMatrix, SaveExchangeMatrix(MeshNo)%DPSave &
    )
    CALL AllocSet(RNR_EM, SaveRNR_EM(MeshNo)%IntSave)
    CALL AllocSet(CNR_EM, SaveCNR_EM(MeshNo)%IntSave)
    CALL AllocSet(SDNR_EM, SaveSDNR_EM(MeshNo)%IntSave)

    Savenze4(MeshNo)=nze4
    CALL AllocSet(YA4, SaveYA4(MeshNo)%DPArrSave)
    CALL AllocSet(FAX, SaveFAX(MeshNo)%DPSave)
    CALL AllocSet(FAY, SaveFAY(MeshNo)%DPSave)
    CALL AllocSet(FAZ, SaveFAZ(MeshNo)%DPSave)
    CALL AllocSet(RNR4,  SaveRNR4(MeshNo)%IntSave)
    CALL AllocSet(CNR4,  SaveCNR4(MeshNo)%IntSave)
    CALL AllocSet(SDNR4, SaveSDNR4(MeshNo)%IntSave)

    Savenze_im(MeshNo)=nze_im
    CALL AllocSet( &
      InterpolationMatrix, SaveInterpolationMatrix(MeshNo)%DPSave &
    )
    CALL AllocSet(RNR_IM, SaveRNR_IM(MeshNo)%IntSave)
    CALL AllocSet(CNR_IM, SaveCNR_IM(MeshNo)%IntSave)
    CALL AllocSet(SDNR_IM, SaveSDNR_IM(MeshNo)%IntSave)

    CALL AllocSet(BA, SaveBA(MeshNo)%DPArrSave)
    CALL AllocSet(BoundaryNodeIndex, SaveBoundaryNodeIndex(MeshNo)%IntSave)
  END SUBROUTINE SaveFEM


    !---------------------------------------------------------------		
    !   LoadFEM(MeshNo) loads all important global variables
    !                    of the FEM
    !                    The index of the FEMState  is MeshNo
    !---------------------------------------------------------------

  SUBROUTINE LoadFEM(MeshNo)
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: MeshNo


    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(Savenze_pnm).EQV..FALSE.)  THEN
      Write(*,*) ' No FEM data saved ... '
      RETURN
    ENDIF


    nze_pnm = Savenze_pnm(MeshNo)
    CALL AllocSet( &
      SavePoissonNeumannMatrix(MeshNo)%DPSave, PoissonNeumannMatrix &
    )
    CALL AllocSet(SaveRNR_PNM(MeshNo)%IntSave, RNR_PNM)
    CALL AllocSet(SaveCNR_PNM(MeshNo)%IntSave, CNR_PNM)
    CALL AllocSet(SavePNM_RWORK(MeshNo)%DPSave,  PNM_RWORK)
    CALL AllocSet(SavePNM_IWORK(MeshNo)%IntSave, PNM_IWORK)

    nze_pdm = Savenze_pdm(MeshNo)
    CALL AllocSet( &
      SavePoissonDirichletMatrix(MeshNo)%DPSave, PoissonDirichletMatrix &
    )
    CALL AllocSet(SaveRNR_PDM(MeshNo)%IntSave, RNR_PDM)
    CALL AllocSet(SaveCNR_PDM(MeshNo)%IntSave, CNR_PDM)
    CALL AllocSet(SavePDM_RWORK(MeshNo)%DPSave,  PDM_RWORK)
    CALL AllocSet(SavePDM_IWORK(MeshNo)%IntSave, PDM_IWORK)

    nze_em = Savenze_em(MeshNo)
    CALL AllocSet( &
      SaveExchangeMatrix(MeshNo)%DPSave, ExchangeMatrix &
    )
    CALL AllocSet(SaveRNR_EM(MeshNo)%IntSave, RNR_EM)
    CALL AllocSet(SaveCNR_EM(MeshNo)%IntSave, CNR_EM)
    CALL AllocSet(SaveSDNR_EM(MeshNo)%IntSave, SDNR_EM)

    nze4 = Savenze4(MeshNo)
    CALL AllocSet(SaveYA4(MeshNo)%DPArrSave, YA4)
    CALL AllocSet(SaveFAX(MeshNo)%DPSave, FAX)
    CALL AllocSet(SaveFAY(MeshNo)%DPSave, FAY)
    CALL AllocSet(SaveFAZ(MeshNo)%DPSave, FAZ)
    CALL AllocSet(SaveRNR4(MeshNo)%IntSave,  RNR4)
    CALL AllocSet(SaveCNR4(MeshNo)%IntSave,  CNR4)
    CALL AllocSet(SaveSDNR4(MeshNo)%IntSave, SDNR4)

    nze_im = Savenze_im(MeshNo)
    CALL AllocSet( &
      SaveInterpolationMatrix(MeshNo)%DPSave, InterpolationMatrix &
    )
    CALL AllocSet(SaveRNR_IM(MeshNo)%IntSave, RNR_IM)
    CALL AllocSet(SaveCNR_IM(MeshNo)%IntSave, CNR_IM)
    CALL AllocSet(SaveSDNR_IM(MeshNo)%IntSave, SDNR_IM)

    CALL AllocSet(SaveBA(MeshNo)%DPArrSave, BA)
    CALL AllocSet(SaveBoundaryNodeIndex(MeshNo)%IntSave, BoundaryNodeIndex)
  END SUBROUTINE LoadFEM

END MODULE Finite_Element
