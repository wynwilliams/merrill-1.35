PROGRAM Minimize_Bench
    USE Merrill
    IMPLICIT NONE

    CHARACTER(len=1024) :: program_name
    CHARACTER(len=1024) :: mesh_filename
    INTEGER :: n_reps

    INTEGER(KIND=8) :: start_count, stop_count
    INTEGER(KIND=8) :: count_rate

    CHARACTER(len=1024) :: argument
    INTEGER :: i


    CALL GET_COMMAND_ARGUMENT(0, program_name)

    ! Parse command line arguments
    IF(COMMAND_ARGUMENT_COUNT() .NE. 2) THEN
        WRITE(*,*) "USAGE: ", TRIM(program_name), " MESH_FILE N_REPS"
        STOP 1
    END IF

    ! MESH_FILE
    CALL GET_COMMAND_ARGUMENT(1, argument)
    mesh_filename = TRIM(argument)

    ! N_REPS
    CALL GET_COMMAND_ARGUMENT(2, argument)
    READ(argument, '(I10)') n_reps


    CALL InitializeMerrill()

    CALL Magnetite(20.0d0)

    !meshfile=TRIM(mesh_filename)
    CALL ReadPatranMesh(TRIM(mesh_filename))


    ! Set Ls so the 0.1um octahedron is 0.2um, in vortex range.
    WRITE(*,*) "Old Ls: ", Ls
    Ls = 1e12 / 4
    WRITE(*,*) "New Ls: ", Ls

    WRITE(*,*) "RUNNING EnergyMin"
    CALL SYSTEM_CLOCK(start_count, count_rate)
    DO i=1, n_reps
        m = 1/sqrt(3.0d0)
        CALL EnergyMin()
    END DO
    CALL SYSTEM_CLOCK(stop_count)

    WRITE(*,*)
    WRITE(*,*) TRIM(program_name), " elapsed time: ", &
        REAL(stop_count - start_count)/REAL(count_rate)
    WRITE(*,*)
END PROGRAM Minimize_Bench
