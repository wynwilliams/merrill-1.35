!
! Test CalcDemagEx conforms to some known analytic solutions.
!
PROGRAM Demag_Test
  USE Merrill
  !$ USE omp_lib
  IMPLICIT NONE

  CHARACTER(len=1024) :: program_name
  CHARACTER(len=1024) :: sphere_mesh_filename

  CHARACTER(len=1024) :: argument

  LOGICAL :: test_passed

  REAL(KIND=DP) :: sphere_radius
  REAL(KIND=DP) :: old_Ms
  INTEGER :: i


  CALL GET_COMMAND_ARGUMENT(0, program_name)

  ! Parse command line arguments
  IF(COMMAND_ARGUMENT_COUNT() .NE. 1) THEN
    WRITE(*,*) "USAGE: ", TRIM(program_name), " SPHERE_MESH_FILE"
    STOP 1
  END IF

  ! SPHERE_MESH_FILE
  CALL GET_COMMAND_ARGUMENT(1, argument)
  sphere_mesh_filename = TRIM(argument)


  CALL InitializeMerrill()

  CALL Magnetite(20.0d0)

  CALL ReadPatranMesh(TRIM(sphere_mesh_filename))

  ! Find sphere radius
  sphere_radius = MAXVAL(NORM2(VCL(:,1:3), DIM=2))


  !
  ! Run single-phase sphere demag tests
  !

  !$OMP PARALLEL
  !$OMP MASTER
  !$ WRITE(*,*) "N THREADS: ", omp_get_num_threads()
  !$OMP END MASTER
  !$OMP END PARALLEL

  !
  ! Test H = - M / 3 for uniform M in a sphere
  !
  CALL TestUniformSphere( &
    Ms(1)*(/ 1/SQRT(3.0d0), 1/SQRT(3.0d0), 1/SQRT(3.0d0) /), &
    (/ 0.0_DP, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 1.0d0, 0.0d0, 0.0d0 /), &
    (/ 0.0_DP, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 0.0d0, 1.0d0, 0.0d0 /), &
    (/ 0.0_DP, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 0.0d0, 0.0d0, 1.0d0 /), &
    (/ 0.0_DP, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 1/SQRT(2.0d0), 1/SQRT(2.0d0), 0.0d0 /), &
    (/ 0.0_DP, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE) /) &
  )


  !
  ! Run multi-phase sphere demag tests
  !

  ! Run tests for two duplicate spheres

  old_Ms = Ms(1)
  CALL BuildDuplicateSphere()

  Ms(:) = (/ old_Ms, 0.0_DP /)
  CALL TestUniformSphere( &
    Ms(1)*(/ 1/SQRT(3.0d0), 1/SQRT(3.0d0), 1/SQRT(3.0d0) /), &
    (/ -sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE/2) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 1.0d0, 0.0d0, 0.0d0 /), &
    (/ -sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE/2) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 0.0d0, 1.0d0, 0.0d0 /), &
    (/ -sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE/2) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 0.0d0, 0.0d0, 1.0d0 /), &
    (/ -sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE/2) /) &
  )
  CALL TestUniformSphere( &
    Ms(1)*(/ 1/SQRT(2.0d0), 1/SQRT(2.0d0), 0.0d0 /), &
    (/ -sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=1, NNODE/2) /) &
  )

  Ms(:) = (/ 0.0_DP, old_Ms /)
  CALL TestUniformSphere( &
    Ms(2)*(/ 1/SQRT(3.0d0), 1/SQRT(3.0d0), 1/SQRT(3.0d0) /), &
    (/ +sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=NNODE/2+1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(2)*(/ 1.0d0, 0.0d0, 0.0d0 /), &
    (/ +sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=NNODE/2+1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(2)*(/ 0.0d0, 1.0d0, 0.0d0 /), &
    (/ +sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=NNODE/2+1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(2)*(/ 0.0d0, 0.0d0, 1.0d0 /), &
    (/ +sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=NNODE/2+1, NNODE) /) &
  )
  CALL TestUniformSphere( &
    Ms(2)*(/ 1/SQRT(2.0d0), 1/SQRT(2.0d0), 0.0d0 /), &
    (/ +sphere_radius, 0.0_DP, 0.0_DP /), sphere_radius, &
    (/ (i, i=NNODE/2+1, NNODE) /) &
  )


  ! Test for solutions with random air segments
  CALL TestRandomAir()

CONTAINS

  ! Test solutions for uniform M in a sphere
  SUBROUTINE TestUniformSphere(test_M, sphere_center, sphere_radius, nodes)
    REAL(KIND=DP), INTENT(IN) :: test_M(3)
    REAL(KIND=DP), INTENT(IN) :: sphere_center(3)
    REAL(KIND=DP), INTENT(IN) :: sphere_radius
    INTEGER, INTENT(IN) :: nodes(:)

    REAL(KIND=DP) :: expected_h(3), found_h(3)
    REAL(KIND=DP) :: expected_e, found_e

    REAL(KIND=DP) :: r_cos_theta, expected_phi, found_phi
    REAL(KIND=DP) :: center_phi, top_phi, r

    REAL(KIND=DP) :: pi = 4*atan(1.0D0)
    REAL(KIND=DP) :: diff

    INTEGER :: ni, i


    DO i=1,NNODE
      m(i,:) = test_M / NORM2(test_M)
    END DO

    CALL CalcDemagEx()

    center_phi = FindCenterPhi()
    top_phi    = FindTopPhi(test_M/NORM2(test_M))

    test_passed = .TRUE.
    DO ni=LBOUND(nodes,1), UBOUND(nodes,1)
      i = nodes(ni)

      !
      ! Check scalar potential
      !
      ! phi(r, theta) = M/3 * |r| cos(theta)
      !
      r_cos_theta = DOT_PRODUCT(m(i,:), (VCL(i,1:3) - sphere_center))

      expected_phi = 1 / 3.0 * r_cos_theta
      found_phi = totfx(i)
      diff = ABS(expected_phi - found_phi)

      ! Only check reasonably large values. Near the center
      ! and along the central plane pops up a few problematic
      ! values. The number chosen here is just the smallest
      ! number the tests passed for.
      IF(ABS(expected_phi)/ABS(top_phi) .GT. 2e-3) THEN
        diff = diff/ABS(expected_phi)
      END IF

      ! Check only if we're not right on the boundary.
      ! The BEM method isn't super accurate on the boundary.
      r = NORM2(VCL(i,1:3) - sphere_center)
      IF(r/sphere_radius .GT. 0.99) diff = 0

      IF(diff .GT. 3e-2) THEN
        WRITE(*,*) "Test failed for phi at i=", i
        WRITE(*,*) "Expected: ", expected_phi
        WRITE(*,*) "Got:      ", found_phi
        WRITE(*,*) "Frac f/e  ", found_phi/expected_phi
        WRITE(*,*)

        test_passed = .FALSE.
      END IF

      !
      ! Check energy gradient
      !
      ! E = -mu0/2 integral M.H dV
      ! H = -M/3
      ! => E = -mu0/2 integral M.M/3 dV = -mu0/6 Ms^2 integral m.m dV
      ! dE/dm_i = -mu0/3 Ms^2 integral m_i/3 dV
      ! =>      = mu0/3 * Ms^2 m_i * integral dV
      !
      expected_h = (4*pi*1e-7)/3 * NORM2(test_M)**2 * m(i,:) * vbox(i)
      found_h = hdemag(i,:)
      diff = SQRT(SUM((expected_h - found_h)**2)) &
        / SQRT(SUM((expected_h)**2))
      IF(diff .GT. 1e-2) THEN
        WRITE(*,*) "Test failed for H at i=", i
        WRITE(*,*) "Expected: ", expected_h
        WRITE(*,*) "Got:      ", found_h
        WRITE(*,*) "Frac f/e  ", found_h/expected_h
        WRITE(*,*)

        test_passed = .FALSE.
      END IF

      !
      ! Check energy
      !
      ! E = -mu0/2 integral M.H dV
      ! H = -M/3
      ! => M.H = -M.M/3 = -(Ms^2)/3
      ! => E = mu0/2 Ms^2 1/3 * integral dV
      !
      expected_e = &
        4*pi*1e-7/2 * NORM2(test_M)**2 * (1/3.0D0) &
        * (4.0/3.0)*pi*sphere_radius**3
      found_e = DemagEnerg
      diff = ABS(expected_e - found_e) / ABS(expected_e)
      IF(diff .GT. 1e-2) THEN
        WRITE(*,*) "Test failed for E at i=", i
        WRITE(*,*) "Expected: ", expected_e
        WRITE(*,*) "Got:      ", found_e
        WRITE(*,*) "diff:     ", diff
        WRITE(*,*) "Frac f/e  ", found_e/expected_e
        WRITE(*,*)

        test_passed = .FALSE.
      END IF
    END DO

    IF(test_passed .EQV. .FALSE.) THEN
      WRITE(*,*) "Test failed."
      STOP 1
    ELSE
      WRITE(*,*) "Test passed."
    END IF
  END SUBROUTINE TestUniformSphere


  ! Find the scalar potential at the center of the sphere
  FUNCTION FindCenterPhi()
    REAL(KIND=DP) FindCenterPhi
    REAL(KIND=DP) :: l(4)
    INTEGER :: i, j

    ! Find the central tet
    DO i=1,NTRI
      DO j=1,4
        ! Find barycentric coordinates
        l(j) = DOT_PRODUCT( &
                (/ b(i,j), c(i,j), d(i,j) /), &
                (/ 0.0, 0.0, 0.0 /) -VCL(TIL(i,j),1:3) &
            ) / (6*vol(i)) + 1
      END DO

      ! 0,0,0 is insite the tetrahedron if 0 <= l <= 1 for all l
      IF(ALL(l .LE. 1) .AND. ALL(l .GE. 0)) THEN
        ! Generate FindCenterPhi using the barycentric coordinates as weights
        FindCenterPhi = 0
        DO j=1,4
          FindCenterPhi = FindCenterPhi + l(j)*totfx(TIL(i,j))
        END DO

        EXIT
      END IF
    END DO
  END FUNCTION FindCenterPhi

  FUNCTION FindTopPhi(test_m)
    REAL(KIND=DP) :: FindTopPhi
    REAL(KIND=DP) :: test_m(3)
    REAL(KIND=DP) :: r_cos_theta, r_cos_theta_max
    INTEGER :: i
 
    r_cos_theta_max = -HUGE(r_cos_theta_max)
    DO i=1,NNODE
      r_cos_theta = DOT_PRODUCT(test_m, VCL(i,1:3))
      IF(r_cos_theta_max .LT. r_cos_theta) r_cos_theta_max = r_cos_theta
    END DO

    FindTopPhi = r_cos_theta_max
  END FUNCTION FindTopPhi

  SUBROUTINE BuildDuplicateSphere
    INTEGER, ALLOCATABLE :: original_TIL(:,:)
    INTEGER :: original_NTRI
    REAL(KIND=DP), ALLOCATABLE :: original_VCL(:,:)
    INTEGER :: original_NNODE

    REAL(KIND=DP) :: sphere_radius
    INTEGER :: i


    ! Find original sphere radius
    !sphere_radius = 0
    !DO i=1,NNODE
    !  IF(sphere_radius < NORM2(VCL(i,1:3))) sphere_radius = NORM2(VCL(i,1:3))
    !END DO
    sphere_radius = MAXVAL(NORM2(VCL(:,1:3),2))

    ! Duplicate original sphere coords and connectivities
    CALL AllocSet(TIL, original_TIL)
    original_NTRI = NTRI
    CALL AllocSet(VCL, original_VCL)
    original_NNODE = NNODE

    ! Make 2 separate spheres
    CALL MeshAllocate(2*NNODE, 2*NTRI)

    ! Make 1 sphere 2*sphere_radius left, and one 2*sphere_radius right

    ! Copy Connectivities
    TIL = 0
    TIL(:original_NTRI, 1:4)   = original_TIL(:, 1:4)
    TIL(original_NTRI+1:, 1:4) = original_TIL(:, 1:4) + original_NNODE

    ! Copy coordinates
    VCL = 0
    VCL(:original_NNODE, 1:3)   = original_VCL(:, 1:3)
    VCL(original_NNODE+1:, 1:3) = original_VCL(:, 1:3)

    ! Move spheres
    DO i=1,original_NNODE
      VCL(i, 1:3) = VCL(i, 1:3) - (/ sphere_radius, 0.0_DP, 0.0_DP /)
    END DO

    DO i=original_NNODE+1, 2*original_NNODE
      VCL(i, 1:3) = VCL(i, 1:3) + (/ sphere_radius, 0.0_DP, 0.0_DP /)
    END DO

    ! Set subdomains
    TetSubDomains(:original_NTRI)   = 1
    TetSubDomains(original_NTRI+1:) = 2
    NMaterials = 2

    CALL BuildTetrahedralMeshData()
    CALL BuildFiniteElement()
    CALL BuildMaterialParameters()
    CALL BuildEnergyCalculator()
  END SUBROUTINE BuildDuplicateSphere


  ! Test the principle of superposition works for the scalar potential
  ! for air inside the meshed region.
  ! Specifically, phi(M1 + M2) = phi(M1) + phi(M2), for M1, M2 distinct
  ! magnetic regions.
  SUBROUTINE TestRandomAir
    INTEGER :: i
    REAL(KIND=DP), ALLOCATABLE :: phi1(:), phi2(:), phitot(:)

    ! 2 materials: Magnetic (1) and air (2)
    NMaterials = 2
    TetSubDomains = 1

    ! Make some air
    DO i=1,NTRI
      IF(MOD(i, 3) .EQ. 0) TetSubDomains(i) = 2
    END DO

    CALL BuildTetrahedralMeshData()
    CALL BuildFiniteElement()
    CALL BuildMaterialParameters()
    CALL BuildEnergyCalculator()

    ALLOCATE(phi1(NNODE), phi2(NNODE), phitot(NNODE))

    ! Set uniform magnetization
    DO i=1,NNODE
      m(i,:) = (/ 1.0_DP, 0.0_DP, 0.0_DP /)
    END DO

    ! Calculate demag for all magnetite
    Ms = 10.0_DP
    CALL CalcDemagEx()

    phitot = totfx


    ! Calculate scalar potential for SD 1 magnetite
    CALL Magnetite(20.0_DP)
    Ms = (/ 10.0_DP, 0.0_DP /)
    CALL CalcDemagEx()

    phi1 = totfx


    ! Calculate scalar potential for SD 2 magnetite
    CALL Magnetite(20.0_DP)
    Ms = (/ 0.0_DP, 10.0_DP /)
    CALL CalcDemagEx()

    phi2 = totfx


    ! Test htot = h1 + h2
    test_passed = .TRUE.
    DO i=1,NNODE
      IF(ABS(phitot(i) - (phi1(i) + phi2(i)))/ABS(phitot(i)) .GT. 6e-4) THEN
        WRITE(*,*) "i", i
        WRITE(*,*) "phitot:    ", phitot(i)
        WRITE(*,*) "phi1:      ", phi1(i)
        WRITE(*,*) "phi2:      ", phi2(i)
        WRITE(*,*) "phi1+phi2: ", phi1(i)+phi2(i)
        WRITE(*,*) "tot/(1+2): ", phitot(i) / (phi1(i)+phi2(i))
        WRITE(*,*) "absdiff:   ", ABS((phitot(i) - (phi1(i)+phi2(i)))/phitot(i))
        WRITE(*,*)
        test_passed = .FALSE.
      END IF
    END DO

    IF(test_passed .EQV. .FALSE.) THEN
      WRITE(*,*) "Test failed."
      STOP 1
    ELSE
      WRITE(*,*) "Test passed."
    END IF
  END SUBROUTINE TestRandomAir

END PROGRAM Demag_Test
