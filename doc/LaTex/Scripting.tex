\section{Script Commands}\label{sec:Scripts}

\subsection{Introduction}
The script is a simple ASCII file containing a sequence of lines. Empty lines, leading, trailing, and multiple spaces or tabs are ignored, as well as anything behind an exclamation mark (!). A number of keywords are used to call subroutines or perform simple assignments. All keywords are case insensitive, e.g. ’ReadMesh’ and ’readmesh’ are equivalent. The script file is parsed line by line. Each valid line is immediately evaluated and executed.

New or modified functions \textcolor{red}{are highlighted in red}.

\subsection{Commands}

\textbf{Set $<$variable$>$ $<$value$>$} is used to define global variables for the material, the geometry of the mesh, or program parameters. The following variables are supported:

\begin{addmargin}[1cm]{1cm} % Indent

\textbf{Ms} saturation magnetization in A/m.

\textbf{K1} 1st anisotropy constant for uniaxial or cubic anisotropy in J/m$^3$.

\textcolor{red}{\textbf{K2} 2nd anisotropy constant for uniaxial or cubic anisotropy in J/m$^3$.}

\textbf{Aex}  exchange constant in J/m.

\textbf{Ls} inverse length scale 1/m. Internally Ls$^2$ is used.

\textbf{mu} related to permeability of free space via mu = $\mu_0/4\pi$ .

\textbf{NEBSpring}  Spring constant for nudged-elastic band method (NEB) .

\textbf{CurvatureWeight}  Weight of curvature contribution for nudged-elastic band method (NEB).

\textbf{MaxMeshNumber}  Maximal number of finite element meshes stored. Must be set once before loading meshes.

\textbf{PathN} Number of structures along the magnetization path.  \textit{Warning}:  The mesh must have been defined previously -- Use only after ReadMesh.

\textbf{ExchangeCalculator} Chooses the exchange energy discretization method used. The available choices are $1 = m\Delta{m}$, $2 = \phi^2$  along edges, $3 = \left(\nabla{\theta}\right)^2 + \sin^2\left(\theta\right) \left(\nabla\phi\right)^2$.

\textbf{MaxRestarts} Maximum number of restarts during energy minimization.

\textbf{MaxEnergyEvaluations}  Maximum number of energy calculations during energy minimization (typical: 10000). Afterwards energy minimization is aborted.

\textbf{MaxPathEvaluations} Maximum number of path energy calculations during path minimization (typical: 2000).

\textbf{Zone} Current Zone to be written into the TecPlot output file (double).   Zone can be set before each output, or can be used with automatic increment.

\textbf{ZoneIncrement} Automatic increment of zone (default=1.0).

\end{addmargin}
\vspace{0.5cm}

\textcolor{red}{\textbf{SetSubDomain  $<$subdomain number$>$ $<$variable name$>$  $<$value$>$},  for multi-phase models. For example,  to set Ms to 4.8e5 for subdomain 2  use}

\textcolor{red}{\hspace{0.5cm}SetSubDomain 2 Ms 4.8e5}

\textcolor{red}{Note that the SubDomain number must be identical to a block number defined in your PATRAN mesh.}

\textbf{Magnetite $<$temperature$>$  C} Defines material constants for magnetite at temperature $<$temperature$>$   to be used in subsequent calculations. The temperature is given in degrees Celsius and must be positive and below magnetite’s Curie temperature at 580$^{\circ}$C.


\textcolor{red}{\textbf{Iron $<$temperature$>$  C} Defines material constants for iron at temperature $<$temperature$>$   to be used in subsequent calculations. The temperature is given in degrees Celsius and must be positive and below iron’s Curie temperature at 770$^{\circ}$C.}

 
\textbf{Resize $<$old length$>$ $<$new length$>$} changes the length scale of the mesh such that after this rescaling the length $<$old length$>$  will become $<$new length$>$  .
(cubic |  uniaxial) anisotropy Defines the symmetry of the anisotropy energy.

\textcolor{red}{\textbf{CubicRotation $<$alpha$>$ $<$theta$>$ $<$phi$>$} Performs a 3D rotation of the cubic anisotropy axis. Variables are real numbers that specify the rotation angles in radians. Alpha, theta, phi, corresponds to rotations around the x-, y-, and z-axes, respectively. Positive angles correspond to rotation in a clockwise sense when viewed from the origin along the specified axis.\\\\
To rotate [111] to [100]:\\
CubicRotation 0 0.6154 -0.7854\\
To rotate [111] to [110]:\\
CubicRotation -0.4636 0.4205 0.1007}

\textcolor{red}{\textbf{CubicRotation $<$alpha$>$ $<$theta$>$ $<$phi$>$ SD = $<$Material number} where $<$Material Number$>$ is an integer that applies to rotation to a specified material of a multi-phase model. Alpha, theta, phi, corresponds to rotations around the x-, y-, and z-axes, respectively. Note you need to include the “SD = “, where SD stands for Sub Domain and a space is needed between ‘SD’, ‘=’, and the material number. The variable it is set to MUST match a BLOCK ID set in your PATRAN mesh file. }

\textbf{Easy axis $<$x$>$ $<$y$>$ $<$z$>$}
Sets the easy axis for uniaxial anisotropy.

\textcolor{red}{\textbf{Easy axis $<$x$>$ $<$y$>$ $<$z$>$ SD = $<$Material number$>$}
Sets the easy axis for uniaxial anisotropy per subdomain in a multi-phase model.}

\textbf{External field direction $<$x$>$ $<$y$>$ $<$z$>$}
Sets the direction vector of an external homogeneous magnetic field. Intrinsically sets the field to B = 1 T.

\textbf{External field strength $<$B$>$ $<$unit$>$}
Sets  the strength  of  the  external  homogeneous  magnetic  field as B in units of unit. Possible values for unit are ’muT’, ’mT’, or ’T’. Must be set after defining the direction. Subsequent calls reset the field to B without changing the direction. Can be used for hysteresis modeling.


\textbf{ReadMesh $<$index$>$ $<$filename$>$ [filetype = $<$filetype$>$ ]} Reads the Patran file $<$filename$>$, and stores the corresponding mesh and finite element arrays at location $<$index$>$. The index must be less or equal to the previously set MaxMeshNumber.

\textcolor{red}{Optionally filetype = $<$filetype$>$  can be specified where $<$filetype$>$  takes the value  patran or tecplot. If not specified patran is assumed. Both block and point format tecplot files are supported e.g.,\\
ReadMesh 1 tecplotfile.tec  FileType  = tecplot\\
Note the required space between FileType and =.}

\textbf{LoadMesh $<$index$>$} Loads a previously read mesh and its finite element arrays from location $<$index$>$.
This mesh will then be used in subsequent operations.

\textbf{ReadMagnetization $<$filename$>$} Reads a magnetization file (.dat or .restart) into the current mesh magnetization array. Make sure that it was created for the currently active mesh! The magnetization read is used in subsequent operations.

\textbf{Uniform magnetization $<$x$>$  $<$y$>$ $<$z$>$}  Creates a uniform magnetization for the current mesh pointing in the normalized direction $<$x$>$,$<$ y$>$, $<$z$>$. The optional parameter b is the index of the block of nodes in the mesh that should be set. By definition block 1 contains the free nodes, while higher block numbers can be used to define fixed nodes. These block have to be defined in the Patran file. Any previous magnetization is lost!

\textcolor{red}{\textbf{Uniform magnetization $<$x$>$  $<$y$>$ $<$z$>$  SD  = $<$Material number$>$} Creates a uniform magnetization for the current mesh pointing in the normalized direction $<$x$>$,$<$ y$>$, $<$z$>$ where $<$Material Number$>$ is an integer that applies the uniform direction to a specified material of a multi-phase model. Note you need to include the SD  = , where SD stands for Sub Domain and a space is needed between ‘SD’ and ‘=’. The variable it is set to MUST match a BLOCK ID set in your PATRAN mesh file.}

\textbf{Randomize magnetization $<$angle$>$} Randomly changes each current magnetization vector by at most $<$angle$>$ degrees. The previous magnetization is (partly) lost! 

\textcolor{red}{\textbf{Randomize magnetization $<$angle$>$ SD  = $<$Material number$>$} Randomly changes each current magnetization vector by at most $<$angle$>$ degrees, where $<$Material Number$>$ is an integer that applies randomisation to a specified material of a multi-phase model. Note you need to include the SD  = , where SD stands for Sub Domain and a space is needed between ‘SD’ and ‘=’. The variable it is set to MUST match a BLOCK ID set in your PATRAN mesh file.}

\textbf{Randomize all moments} Replaces the current magnetization by randomly distributed unit vectors. Any previous magnetization is lost!


\textcolor{red}{\textbf{Randomize all moments $<$angle$>$ SD  = $<$Material Number$>$} Replaces the current magnetization of specified subdomain by randomly distributed unit vectors, where <Material Number > is an integer that applies to randomisation to a specified material of a multi-phase model. Note you need to include the SD  = , where SD stands for Sub Domain and a space is needed between ‘SD’ and ‘=’. The variable it is set to MUST match a BLOCK ID set in your PATRAN mesh file.}

\textbf{ReMesh $<$index$>$} Takes the current magnetization array and interpolates it at the nodes of the previously read mesh at location $<$index$>$. This mesh from location $<$index$>$ is then loaded and will be used in subsequent operations.

\textbf{ConjugateGradient}   Uses conjugate gradient steps during the accelerated descent.

\textbf{SteepestDescent} Uses normal gradient steps during the accelerated descent.

\textbf{Minimize} Calls the minimization routine for the current mesh and initial magnetization. This call does not save the final result!

\textbf{EnergyLog $<$filename$>$} Starts logging all subsequent energy calculations into the logfile $<$filename$>$.log. \textcolor{red}{For multi-phase material the magnetization reported at each node is the box volume average of relative Ms*m for each of the elements to which the node belongs.}  Logging can be stopped by EndLog or CloseLogfile

\textbf{CloseLogfile} Ends the previous logging of energy calculations or path minimizations.

\textbf{WriteMagnetization $<$filename$>$ \textcolor{red}{[$<$packing$>$ ]}} Saves the magnetization and the mesh. This produces two files:
%\begin{addmargin}[1cm]{1cm} % Indent
\textbf{$<$filename$>$}.dat contains vertex coordinates and magnetization vectors only, and 
\textbf{$<$filename$>$}\_mult.dat which is TecPlot format file containing the data and the mesh for visualization using ParaView or TecPlot. Contains mesh geometry and one or more magnetization states.

\textcolor{red}{The optional $<$packing$>$  parameter affects only $<$filename$>$\_mult.dat output. $<$packing$>$  takes the value of either BLOCK or POINT.  BLOCK format is required to save the sub-domain regions of a multi-phase solution to a Tecplot file (readable by Paraview). The subdomains are numbered by an ‘SD’ variable for each element. BLOCK is now the default output format. Note that the $<$filename$>$.dat is unchanged (point) format and contains no subdomain information.}
%\end{addmargin}
\vspace{0.5cm}

\textcolor{red}{\textbf{WriteDemag $<$filename$>$ [$<$packing$>$ ]} Saves the internal grain demagnetization field,  magnetisation (normalised for each material region) and the mesh to a : 
$<$filename$>$\_ demag.tec TecPlot file for visualization using ParaView or TecPlot. Contains mesh geometry and demagnetization field for one or more magnetization states.  The output is of the form “x, y, z, Hd\_x, Hd\_y, Hd\_z, M\_x, M\_y, M\_z SD” Saves in POINT or BLOCK format. H values are in units of A/m and M value are unit vector directions multiplied by the local Ms value. Note SD parameter is only output in BLOCK format The demag field is in A/m.\\
The optional $<$packing$>$  parameter takes the value of either BLOCK or POINT.  BLOCK format is required to save the sub-domain regions of a multi-phase solution to a Tecplot file (readable by Paraview). The subdomains are numbered by an ‘SD’ variable for each element. BLOCK is now the default output format.}

\textcolor{red}{\textbf{AppendDemagZone $<$filename$>$ \textcolor{red}{[$<$packing$>$]}} Saves the internal grain demagnetization field,  magnetisation (normalised for each material region) and the mesh to a : 
$<$filename$>$\_ demag.tec TecPlot file in the same way as the WriteDemag command, for visualization using ParaView or TecPlot.  Subsequent calls will write the demag field and magnetization only. This is useful for multiple solutions that use the same mesh (such as field or volume hysteresis). Each Zone can be named using the ZoneName command. Files are written in the TecPlot file format containing the data and the mesh for visualization using TecPlot only. Paraview can currently only reads Tecplot format files that contain only \textbf{one} zone, and so files created using this command generally cannot be read by paraview. }

\textcolor{red}{The optional $<$packing$>$  parameter takes the value of either BLOCK or POINT.  BLOCK format is required to save the sub-domain regions of a multi-phase solution to a Tecplot file. The subdomains are numbered by an ‘SD’ variable for each element. BLOCK is now the default output format.}
%\end{addmargin}

\textcolor{red}{\textbf{ZoneName $<$string$>$} Provides a Zone name that is written to any Tecplot format file, so works with command WriteMagnetization, WriteDemag and AppendTecplotZone. The string is limited to a maximum of 80 characters.}


\textbf{WriteHyst $<$filename$>$} Saves hysteresis data in 5 columns of  M⋅H$_{ext}$, |H| and the 3 components on the average unit M vector,  where M is the magnetization and H$_{ext}$ is the external field. M⋅H$_{ext}$ is normalized to the saturated magnetization in the direction of the applied field. For multi-phase material the magnetization at each node is the box volume average of relative Ms*m  for each of the elements to which the node belongs. The first line of the file states the total saturated magnetization of the model, used to normalize the reported M⋅H$_{ext}$ values.   Output file is $<$filename $>$.hyst

\textcolor{red}{\textbf{WriteLoopData $<$filename$>$ $<$Name1$>$ $<$Value1$>$ $<$Name2$>$ $<$Value2$>$...} Save field, moment, total volume and user specified variables to file $<$filename$>$.loop. $<$Name1$>$ specified the column header printed to the file and must not contain spaces. $<$Value1$>$ is the real number parameter value to be saved. Useful when looping over user defined variables.\\
Example usage:\\
Loop Br -100 10 100\\
WriteLoopData MyOuputFile Reversal\_Field \$Br\$\\
EndLoop}

\textbf{WriteBoxData $<$filename $>$} Writes the magnetization per node along with the node associated volume x,y,z, m$_x$, m$_y$, m$_z$, vbox. Where x,y,x is the node location, m$_x$, m$_y$, m$_z$  is the magnetization unit vector components, and vbox is the volume associated with a node (in units of microns3). This is useful if you want to compute the absolute magnetization associated with each node. 

\textbf{MagnetizationToPath $<$index $>$} Saves the current magnetization in the path at location $<$index$>$. This allows to assemble a path from individual magnetization states that have to fit to the current mesh! After assembling a path it must be renewed before further operations can be performed.

\textbf{PathToMagnetization $<$index$>$} Moves the path magnetization state at location $<$index$>$ to the current magnetization. This allows to change individual magnetizations in the path. E.g. Initial and final states of a path read from a file can be minimized for new material constants.

\textbf{RenewPath} Defines all path variables, like distances and tangent vectors, assuming that all magnetizations have been correctly filled.

\textbf{RefinePathTo $<$newlength$>$}  Refines the current path to a new number of states by linear interpolation in the magnetization angles. This also resets PathN to the new value and renews the path. Of course, the new number of states can also be less than the previous PathN .

\textbf{WriteTecPlotPath $<$filename$>$  \textcolor{red}{[$<$packing$>$ ]}} Exports the current path to a TecPlot file with name $<$filename$>$ . All states along the path are individual zones in the TecPlotFile.

\textcolor{red}{The optional $<$packing$>$  parameter  takes the value of either BLOCK or POINT.  BLOCK format is required to save the sub-domain regions of a multi-phase solution to a Tecplot file (readable by Paraview). The subdomains are numbered by an ‘SD’ variable for each element. BLOCK is now the default output format.}

\textbf{ReadTecPlotPath $<$filename$>$} Reads a new path from a TecPlot file with name $<$filename$>$. All states along the path are individual zones in the TecPlotFile. Because this also reads in the mesh, all mesh related quantities are recalculated. Make sure that all material parameters are correctly assigned, since those are not read !

\textcolor{red}{\textbf{ReadTecPlotZone $<$filename$>$ $<$zone number$>$} Reads one zone from a TecPlot file with name $<$filename$>$  and makes this the current magnetization. Can be used to read in an initial guess or as a start/end point of an initial path for NEB calculations. No mesh information is read so make sure  the mesh appropriate to the data is already loaded.}

\textcolor{red}{\textbf{AppendTecplotZone $<$filename$>$ \textcolor{red}{[$<$packing$>$]}} Saves the magnetization and the mesh to a file  \textbf{$<$filename$>$}\_mult.dat. For the first call to this command, or when the filename is changed, it will write the magnetization and the finite-element mesh. Subsequent calls will write the magnetization only. This is useful for multiple solutions that use the same mesh (such as field or volume hysteresis). Each Zone can be named using the ZoneName command. Files are written in the TecPlot file format containing the data and the mesh for visualization using TecPlot only. Paraview can currently only reads Tecplot format files that contain only \textbf{one} zone, and so files created using this command generally cannot be read by paraview. }

\textcolor{red}{The optional $<$packing$>$  parameter takes the value of either BLOCK or POINT.  BLOCK format is required to save the sub-domain regions of a multi-phase solution to a Tecplot file. The subdomains are numbered by an ‘SD’ variable for each element. BLOCK is now the default output format.}
%\end{addmargin}

\textcolor{red}{\textbf{ZoneName $<$string$>$} Provides a Zone name that is written to any Tecplot format file, so works with command WriteMagnetization, WriteDemag and AppendTecplotZone. The string is limited to a maximum of 80 characters.}

\textbf{KeyPause} Pauses script evaluation and waits for Key+Enter for continuation.

\textbf{MakeInitialPath} Assumes that a path is defined by set PathN $<$number $>$ and that the first and last magnetization patterns are defined. Then proceeds by stepwise minimization to construct an initial path for subsequent optimization by the NEB method.

\textbf{PathMinimize}  Assumes that an initial path is defined and minimizes the action integral using a variant of the NEB method.

\textbf{PathLogfile $<$filename$>$} Starts logging all subsequent path minimization calculations into three logfiles $<$filename$>$.enlog $<$filename$>$.grlog, and $<$filename$>$.dlog. They contain energies along the path, norms of the gradients along the path and cumulative distances along the path. Logging can be stopped by EndLog or CloseLogfile.

\textbf{SystemCommand $<$command$>$...} Performs the system command in the remaining arguments as a line. No guarantee can be given for correct behavior. Uses FORTRAN’s SYSTEM command.

\textbf{PathStructureEnergies $<$filename$>$} Computes the energies for each structure along the minimum energy path. The output is written to $<$filename$>$, however if this is omitted then the output is written to standard out.

\textcolor{red}{\textbf{ReportEnergy} Makes a report on the model parameters (mesh size, material parameters, exchange length etc, as well as the component and total magnetic energy both in reduced units of (Kd. V) and in Joules.}

\textbf{(Stop |  End)}  Stops script evaluation.

\vspace{0.5cm}

The following commands are advanced options for scripts using loops or variables. 

\textbf{Loop $<$variable $>$ $<$startvalue $>$ $<$endvalue $>$ [$<$step$>$]} Takes all commands until the next EndLoop statement and performs a loop over the enclosed commands by replacing the variable $<$variable$>$ with values from $<$startvalue $>$ to $<$endvalue $>$ in steps of $<$step $>$. If step is not given stepsize step=1.0 is assumed.  Within the loop the string \#$<$variable$>$ is replaced by the integer value of variable, the string \%$<$variable$>$ is replaced by the double precision value of variable , and the string
\$$<$variable$>$\$ is replaced by a string of the value of variable . Nested loops are not supported! Warning: The Loop command itself must NOT contain variables! This is so because currently the parsing for replacing variables is performed only after unravelling the loops.

\textbf{EndLoop} Delimits the set of commands inside the active loop.

\textbf{Define $<$variable$>$ $<$value $>$} Defines a numeric variable that can be used like a loop variable.

\textbf{AddTo $<$variable$>$ $<$value $>$} Adds a number to a previously defined variable.

\textbf{Undefine $<$variable$>$} Forgets the previously defined variable.
