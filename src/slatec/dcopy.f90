MODULE slatec_dcopy
CONTAINS

!DECK DCOPY
      SUBROUTINE DCOPY (N, DX, INCX, DY, INCY)
!***BEGIN PROLOGUE  DCOPY
!***PURPOSE  Copy a vector.
!***LIBRARY   SLATEC (BLAS)
!***CATEGORY  D1A5
!***TYPE      DOUBLE PRECISION (SCOPY-S, DCOPY-D, CCOPY-C, ICOPY-I)
!***KEYWORDS  BLAS, COPY, LINEAR ALGEBRA, VECTOR
!***AUTHOR  Lawson, C. L., (JPL)
!           Hanson, R. J., (SNLA)
!           Kincaid, D. R., (U. of Texas)
!           Krogh, F. T., (JPL)
!***DESCRIPTION
!
!                B L A S  Subprogram
!    Description of Parameters
!
!     --Input--
!        N  number of elements in input vector(s)
!       DX  double precision vector with N elements
!     INCX  storage spacing between elements of DX
!       DY  double precision vector with N elements
!     INCY  storage spacing between elements of DY
!
!     --Output--
!       DY  copy of vector DX (unchanged if N .LE. 0)
!
!     Copy double precision DX to double precision DY.
!     For I = 0 to N-1, copy DX(LX+I*INCX) to DY(LY+I*INCY),
!     where LX = 1 if INCX .GE. 0, else LX = 1+(1-N)*INCX, and LY is
!     defined in a similar way using INCY.
!
!***REFERENCES  C. L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
!                 Krogh, Basic linear algebra subprograms for Fortran
!                 usage, Algorithm No. 539, Transactions on Mathematical
!                 Software 5, 3 (September 1979), pp. 308-323.
!***ROUTINES CALLED  (NONE)
!***REVISION HISTORY  (YYMMDD)
!   791001  DATE WRITTEN
!   890831  Modified array declarations.  (WRB)
!   890831  REVISION DATE from Version 3.2
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!   920310  Corrected definition of LX in DESCRIPTION.  (WRB)
!   920501  Reformatted the REFERENCES section.  (WRB)
!***END PROLOGUE  DCOPY
      DOUBLE PRECISION DX(*), DY(*)
!***FIRST EXECUTABLE STATEMENT  DCOPY
      IF (N .LE. 0) RETURN
      !IF (INCX .EQ. INCY) IF (INCX-1) 5,20,60
      IF(INCX.NE.INCY .OR. (INCX-1).LT.0) THEN
!
!     Code for unequal or nonpositive increments.
!
        IX = 1
        IY = 1
        IF (INCX .LT. 0) IX = (-N+1)*INCX + 1
        IF (INCY .LT. 0) IY = (-N+1)*INCY + 1
        DO I = 1,N
          DY(IY) = DX(IX)
          IX = IX + INCX
          IY = IY + INCY
        END DO
        RETURN

      ELSE IF((INCX-1).EQ.0) THEN
!
!     Code for both increments equal to 1.
!
!     Clean-up loop so remaining vector length is a multiple of 7.
!
        M = MOD(N,7)
        IF (M .NE. 0) THEN
          DO I = 1,M
            DY(I) = DX(I)
          END DO
          IF (N .LT. 7) RETURN
        END IF
        MP1 = M + 1
        DO I = MP1,N,7
          DY(I) = DX(I)
          DY(I+1) = DX(I+1)
          DY(I+2) = DX(I+2)
          DY(I+3) = DX(I+3)
          DY(I+4) = DX(I+4)
          DY(I+5) = DX(I+5)
          DY(I+6) = DX(I+6)
        END DO
        RETURN

      ELSE !(INCX-1).GT.0
!
!     Code for equal, positive, non-unit increments.
!
        NS = N*INCX
        DO I = 1,NS,INCX
          DY(I) = DX(I)
        END DO
        RETURN
      END IF
      END SUBROUTINE

END MODULE slatec_dcopy
