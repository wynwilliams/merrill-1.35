\contentsline {section}{\numberline {1}Important Considerations}{2}
\contentsline {subsection}{\numberline {1.1}Energy considerations}{2}
\contentsline {subsection}{\numberline {1.2}Meshes}{2}
\contentsline {subsection}{\numberline {1.3}Fluctuation field}{3}
\contentsline {section}{\numberline {2}Script Commands}{4}
\contentsline {subsection}{\numberline {2.1}Introduction}{4}
\contentsline {subsection}{\numberline {2.2}Commands}{4}
\contentsline {section}{\numberline {3}Compiling MERRILL}{11}
\contentsline {subsection}{\numberline {3.1}Windows}{11}
\contentsline {subsubsection}{\numberline {3.1.1}Requirements}{11}
\contentsline {subsubsection}{\numberline {3.1.2}Compile commands}{11}
\contentsline {subsection}{\numberline {3.2}OS X}{11}
\contentsline {subsubsection}{\numberline {3.2.1}Requirements}{11}
\contentsline {subsubsection}{\numberline {3.2.2}Compile commands}{11}
\contentsline {subsection}{\numberline {3.3}Linux}{11}
\contentsline {subsubsection}{\numberline {3.3.1}Requirements}{11}
\contentsline {subsubsection}{\numberline {3.3.2}Compile commands}{11}
