\section{Important Considerations}\label{sec:Considerations}

\subsection{Energy considerations}
Energy minimization is limited by physical, numerical and algorithmic constraints.

\textbf{Numerical constraints} The different minimization algorithms have a  limited  precision.  Minimization below this precision can lead to unwanted repetitions due to numerical noise. This can be related to the conjugate gradient method for the sparse matrices or to limited precision in internal functions, e.g. trigonometric functions used for transforming polar to cartesian coordinates.

\textbf{Algorithmic constraints} The finite mesh size or finite distances introduce grid errors and artifacts. The weak FEM solutions are elements of a finite dimensional space approximating a real physical solution. Minimizing below the corresponding approximation error is physically meaningless, even if mathematically correct.

\textbf{Physical constraints} The micromagnetic model only represents a part of the physically relevant energies. Some energy terms are neglected, like magnetostriction, elastic, and electric energies. Most notably thermal activation is disregarded. Minimization of the energy to higher precision than the real variability of the energy is physically irrelevant.

Sometimes it may be mathematically useful to perform higher precision minimizations. This could be necessary to find a complex minimization route that finally leads to a also physically better minimum.

To benchmark different algorithms it also can be useful to compare their output to unphysical degrees of precision.

In most cases it is useful to have a constantly updated estimate of the different accuracy requests to avoid unnecessary minimization steps. The thermal energy per degree of freedom is 1/2kT . In the FEM model the number of degrees of freedom (DOF) is 2 × NNODE where NNODE is the number of nodes in the
mesh, because each unit vector has two DOF. The total variability in energy density $\Delta$E is then

\[\Delta E = \frac{{kT\;NNODE}}{{{V_{tot}}}}\]



\subsection{Meshes}
The mesh related discretization error can be estimated by comparing results on two separate meshes with similar number of nodes.
To test whether a minimum is really achieved one can monitor largest angular variation along the minimization route. Close to a supposed minimum one can perturb the current state m$_0$ into a state m$_1$ which is a distance d away from m$_0$. If after k minimization steps the result m2 is within $||$m2 - m0$||$ $<$ d/k then m$_0$ is assumed to represent an LEM.

\subsection{Fluctuation  field}
Van de Wiele et al. (2010) state an expression for the fluctuation field in a micromagnetic model where the amplitude of the thermal fluctuations is derived from the fluctuation-dissipation theorem. A corrected and adapted version of this expression for a tetrahedral subvolume is:

\[{H_{therm}} = \sqrt {\frac{{\alpha kT}}{{\gamma E\mu _0^2{M_S}{V_{tet}}\delta t}}} \]
